<?php // -*- c -*-

/* ************************************************************************** */
/*                                                                            */
/*     Copyright (C)	2010 Benjamin Drieu (bdrieu@april.org)		      */
/*                                                                            */
/*  This program is free software; you can redistribute it and/or modify      */
/*  it under the terms of the GNU General Public License as published by      */
/*  the Free Software Foundation; either version 2 of the License, or         */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU General Public License for more details.                              */
/*                                                                            */
/*  You should have received a copy of the GNU General Public License         */
/*  along with this program; if not, write to the Free Software               */
/*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */
/*                                                                            */
/* ************************************************************************** */


function lang ( $string, $params = Array() )
{
  $foo = Array();		// We need this dummy variable to pass
				// as reference to do_translation.
  if ( is_array ( $params ) )
    $params = implode (',', $params );

  return do_translation ( Array ( 'var' => $params ), $string, $foo, $foo );
}



function do_translation ($params, $content, &$smarty, &$repeat)
{
  global $lang;

  if (isset($content)) {
    if ( is_array ( $lang ) && array_key_exists ( $content, $lang ) &&
	 $lang [ $content ] )
      return vsprintf ( $lang [ $content ], explode ( ',', $params [ 'var' ] ) );
    else
      return vsprintf ( $content, explode ( ',', $params [ 'var' ] ) );
  }
}



/**
 * Set the language for user.  Store the chosen language in a session
 * variable.
 *
 * \param $language	ISO-like code for the language.  A file with
 *			this name and .php extension should exist in the
 *			lang/ subdirectory.
 */
function set_language ( $language = 'en' )
{
  if ( ! $language || 
       ! preg_match ( '/^[a-z_]*$/', $language ) )
    $_SESSION [ 'language' ] = 'en';
  else
    $_SESSION [ 'language' ] = $language;
	
  setcookie ( 'dc_language_default', $language, @mktime(0,0,0,12,31,2037,0), '/', GPT_DOMAIN );

  redirect ( $_SERVER [ 'HTTP_REFERER' ] );
}



/**
 * This function assure 
 *
 */
function get_all_languages ( )
{
    $dir = opendir ( GPT_DIR . '/lang' );
    if ( ! $dir )
    {
	croak ( 'Unable to obtain language list' );
    }
    
    $file_list = Array();
    while ( $file = readdir ( $dir ) )
    {
	if ( preg_match ( '/^(.*)\.php$/', $file, $matches ) )
	    array_push ( $file_list, $matches [ 1 ] );
    }
    
    return $file_list;
}



/**
 *
 *
 */
function list_languages ( $args )
{
    global $smarty;
    $smarty -> assign ( 'languages', get_all_languages () );
    $smarty -> display ( 'list_languages.tpl' );
}



/**
 *
 *
 */
function get_translation_strings ( &$files, &$lang ) 
{
    $smarty_files = Array ();
    $php_files = Array ();
    
    /* Smarty files */
    foreach ( Array ( 'templates/smarty', 'templates/mail' ) 
	      as $dirname )
    {
	$dir = opendir ( GPT_DIR . '/' . $dirname );
	while ( $file = readdir ( $dir ) )
	{
	    if ( preg_match ( '/tpl$/', $file ) )
		$smarty_files [] = GPT_DIR . '/' . $dirname . '/' . $file;
	}
    }

    foreach ( $smarty_files as $file )
    {
	$contents = @file_get_contents ( $file );

	preg_match_all ( '/{lang(?: var=[^}]*)?}([^{]*){\/lang}/', $contents, $matches );
	preg_match_all ( '/title="([^"{]*)/', $contents, $matches2 );
	$m = array_merge ( $matches [ 1 ], $matches2 [ 1 ] );

	foreach ( $m as $key )
	{
	    if ( ! $lang || ! array_key_exists ( $key, $lang ) )
	    {
		$lang [ $key ] = '';
	    }
	    $files [ $key ] [] = $file;
	}
    }

    foreach ( Array ( 'index.php', './include/utils.php', './include/language.php', './include/auth.php', './include/mail.php', './include/petition.php' ) as $file )
      {
	$php_files [] = GPT_DIR . '/' . $file;
      }

    /* PHP files */
    $dir = opendir ( GPT_DIR . '/include/' );
    while ( $file = readdir ( $dir ) )
    {
	if ( preg_match ( '/php$/', $file ) )
	    $php_files [] = GPT_DIR . '/include/' . $file;
    }
    $php_files [] = GPT_DIR . '/index.php';
    
    foreach ( $php_files as $file )
    {
	$contents = @file_get_contents ( $file );

	preg_match_all ( '/lang *\( *"([^"]*)"/', $contents, $matches );
	preg_match_all ( '/lang *\( *\'([^\']*)\'/', $contents, $matches2 );
	$m = array_merge ( $matches [ 1 ], $matches2 [ 1 ] );

	foreach ( $m as $key )
	{
	    if ( ! $lang || ! array_key_exists ( $key, $lang ) )
	    {
		$lang [ $key ] = '';
	    }
	    $files [ $key ] [] = $file;
	}
    }
}



/**
 *
 *
 *
 */
function get_updated_language ( $name )
{
    $files = Array();
    $lang = Array ();

    /* Include translation file to keep already translated files, */
    if ( ! preg_match ( '/^[a-z_]+$/', $name ) ||
	 ! file_exists ( sprintf ( '%s/lang/%s.php', GPT_DIR, $name ) ) )
    {
	croak ( 'Invalid language' );
    }
    include ( sprintf ( '%s/lang/%s.php', GPT_DIR, $name ) );

    get_translation_strings ( $files, $lang );

    $string = "<?php\n\n// Language file for GPT\n";
    $string .= sprintf ( "// Updated by %s on %s\n\n", $_SESSION [ 'login' ], strftime('%F %X') );

    $string .= "\$lang = Array ( \n\n";

    foreach ( $lang as $key => $value )
    {
	if ( $files [ $key ] )
	{
	    $string .= sprintf ( "	// In files %s\n", implode ( ', ', array_unique ( $files [ $key ] ) ) );
	    $key = str_replace ( "'", "\'", $key );
	    $value = str_replace ( "'", "\'", $value );
	    $string .= sprintf ( "	'%s' => '%s',\n\n", $key, $value );
	}
    }
    $string .= "	);\n\n";
    $string .=  "?>\n";

    return $string;
}


/**
 *
 *
 */
function get_language ( $name, $updated = FALSE )
{
    if ( ! preg_match ( '/^[a-z_]+$/', $name ) ||
	 ! file_exists ( sprintf ( '%s/lang/%s.php', GPT_DIR, $name ) ) )
    {
	croak ( 'Invalid language' );
    }

    header ( sprintf ( "Content-Disposition: attachment; filename=\"%s.php\"\n", $name ) );
    header ( "Content-type: text/plain\n\n" );

    if ( $updated )
    {
	print get_updated_language ( $name );
    }
    else 
    {
	print @file_get_contents ( sprintf ( '%s/lang/%s.php', GPT_DIR, $name ) );
    }

    exit;
}



function refresh_languages ( )
{
    global $smarty;

    $diagnostic = '';

    foreach ( get_all_languages () as $language_name )
    {
	$content = get_updated_language ( $language_name );
	$fh = @fopen ( sprintf ( '%s/lang/%s.php', GPT_DIR, $language_name ), 'w' );
	if ( ! $fh )
	{
	    $diagnostic .= sprintf ( "<li><img src=\"../images/error.png\"/>&nbsp;Failed to update language %s, <a href=\"?action=get_updated_language&language=%s\">click here</a> to download a copy and manually put it to the <tt>lang/</tt> subdirectory of your GPT installation.</li>", 
				     $language_name, $language_name );
	}
	else 
	{
	    fwrite ( $fh, $content );
	    fclose ( $fh );
	    $diagnostic .= sprintf ( "<li><img src=\"../images/notice.png\"/>Language %s sucessfully updated.</li>", 
				     $language_name, $language_name );
	}
    }
    
    $smarty -> assign ( 'diagnostic', $diagnostic );
    $smarty -> display ( 'refresh_languages.tpl' );
}


?>
