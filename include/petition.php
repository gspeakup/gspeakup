<?php // -*- c -*-

/* ************************************************************************** */
/*                                                                            */
/*     Copyright (C) 2007-2014 Benjamin Drieu (bdrieu@april.org)	      */
/*                                                                            */
/*  This program is free software; you can redistribute it and/or modify      */
/*  it under the terms of the GNU General Public License as published by      */
/*  the Free Software Foundation; either version 2 of the License, or         */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU General Public License for more details.                              */
/*                                                                            */
/*  You should have received a copy of the GNU General Public License         */
/*  along with this program; if not, write to the Free Software               */
/*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */
/*                                                                            */
/* ************************************************************************** */


function petition_show_signatures ( $country, $letter, $type = 'X' )
{
    global $smarty;
    $where = '';

    if ( $type != '2' ) 
      $where .= sprintf ( " AND `signature_type` = '%d' ", maybe_addslashes ( $type ) );

    if ( $type == 0 )
      $smarty -> assign ( 'action', 'signatures-individuals' );
    else if ( $type == 1 )
      $smarty -> assign ( 'action', 'signatures-organizations' );
    else if ( $type == 2 )
      $smarty -> assign ( 'action', 'signatures' );
    
    if ( $country && $country != 'all'  )
	$where .= sprintf ( " AND `country` = '%s' ", maybe_addslashes ( $country ) );
    else
	$country = 'all';

    $smarty -> assign ( 'country', $country );
    $smarty -> assign ( 'letter', $letter );

    if ( $letter )
    {
	$where .= sprintf ( " AND `name` LIKE '%s%%%%' ", maybe_addslashes ( $letter ) );
    }

    $smarty -> assign ( 'signatures',
			simple_query
			( sprintf 
			  ( '  SELECT *, UNIX_TIMESTAMP(validated_time) AS epoch, ' .
			    '         TIMEDIFF(NOW(),validated_time) AS time_ago ' .
			    '    FROM signature ' .
			    '   WHERE validated_time IS NOT NULL ' .
			    $where .
			    'ORDER BY signature_type DESC, organization_name ASC, name ASC ' .
			    '   LIMIT %d,1000;',
			    ( array_key_exists ( 'page', $_GET ) ? 
			      ($_GET [ 'page' ]-1) * 1000 : 0 ) ) ) );
    $smarty -> assign ( 'signatures_count',
			simple_unique_query ( '  SELECT COUNT(signature_id) AS count' .
					      '    FROM signature ' .
					      '   WHERE validated_time IS NOT NULL ' .
					      $where .
					      '    ;' ) );

    $smarty -> display ( 'signatures.tpl' );
}


function petition_check_signature ( $args )
{
  global $smarty;
  
  $fields = Array ( Array ( 'firstname', 'name', 'email' ),
		    Array ( 'organization_name', 'email' ) );


  $missing = Array ( );
  $errors = Array ( );

  if ( ! $args )
    $errors []  = 'Missing all fields !';
  else
    foreach ( $fields [ $args [ 'signature_type' ] ] as $field )
      { 
	if  ( ! array_key_exists ( $field, $args ) || ! $args [ $field ] )
	  {
	    $errors [ $field ] = sprintf ( lang('Missing field "%s"', lang($field) ) );;
	    $missing [ ] = $field;
	  }
      }

  if ( ! preg_match ( '/@/', $args [ 'email' ] ) )
    {
      $errors [ 'email' ] = lang ( 'Invalid mail !' );
    }

  $signature = simple_unique_query ( sprintf ( "SELECT * FROM signature WHERE email = '%s' LIMIT 1;",
					       maybe_addslashes ( $args [ 'email' ] ) ) );
  if ( $signature ) {
      $errors [ ] = lang ('You already have signed the declaration!');
      if ( ! $signature [ 'validated_time' ] )
	  $errors [ ] = lang ('If you want to receive a new validation mail, <a href="%s">click here</a>.',
			      'confirm/' . urlencode ( $args [ 'email' ] ) );
  }

  if ( $errors )
    {
      $smarty -> assign ( 'errors', $errors );
      $smarty -> assign ( 'error', join ( "<br/>", $errors ) );
      return FALSE;
    }

  return TRUE;
}

function petition_validate ( $args )
{
  global $smarty;
  global $_SESSION;

  if ( array_key_exists ( 'HTTP_HTTP_CLIENT_IP', $_SERVER ) )
      $ip = $_SERVER [ 'HTTP_HTTP_CLIENT_IP' ];
  else if ( array_key_exists ( 'REMOTE_ADDR', $_SERVER ) )
      $ip = $_SERVER [ 'REMOTE_ADDR' ];
  else
      $ip = '127.0.0.1';

  $key = petition_generate_token ( $args );

  if ( $args [ 'signature_type' ] == 0 )
  {
      $q = sprintf ( " INSERT INTO signature " .
		     "           ( `signed_time`, `ip`, `ip_country`, `referer`, `key`, `email`, `occupation`, " .
		     "             `firstname`, `name`, `country`, `keep_mail`, `show_signature`, `signature_type` ) " .
		     "    VALUES ( NOW(), INET_ATON('%s'), '%s', %s, '%s', '%s', '%s', ".
		     "             '%s', '%s', '%s', '%d', '%d', '%d' ); ",
		     $ip, 'X', 
		     ( array_key_exists ( 'referer', $_SESSION ) ? "'".maybe_addslashes($_SESSION [ 'referer' ])."'" : 'NULL' ), 
		     maybe_addslashes ( $key ), 
		     maybe_addslashes ( $args [ 'email' ] ),
		     maybe_addslashes ( $args [ 'occupation' ] ),
		     maybe_addslashes ( $args [ 'firstname' ] ),
		     maybe_addslashes ( $args [ 'name' ] ),
		     maybe_addslashes ( $args [ 'country' ] ),
		     ( array_key_exists ( 'keep_mail', $args ) ? maybe_addslashes ( $args [ 'keep_mail' ] ) : '0' ),
		     ( array_key_exists ( 'hide_signature', $args ) ? 0 : 1 ),
		     maybe_addslashes ( $args [ 'signature_type' ] ) );
  }
  else
  {
      $q = sprintf ( " INSERT INTO signature " .
		     "           ( `signed_time`, `ip`, `ip_country`, `key`, `email`, " .
		     "             `organization_name`, `organization_description`, `organization_website`, " .
		     "             `country`, `keep_mail`, `show_signature`, `signature_type` ) " .
		     "    VALUES ( NOW(), INET_ATON('%s'), '%s', '%s', '%s', ".
		     "             '%s', '%s', '%s', " .
		     "             '%s', '%d', '%d', '%d' ); ",
		     $ip, 'X', $key, maybe_addslashes ( $args [ 'email' ] ),
		     maybe_addslashes ( $args [ 'organization_name' ] ),
		     maybe_addslashes ( $args [ 'organization_description' ] ),
		     maybe_addslashes ( $args [ 'organization_website' ] ),
		     maybe_addslashes ( $args [ 'country' ] ),
		     ( array_key_exists ( 'keep_mail', $args ) ? maybe_addslashes ( $args [ 'keep_mail' ] ) : '0' ),
		     ( array_key_exists ( 'hide_signature', $args ) ? 0 : 1 ),
					maybe_addslashes ( $args [ 'signature_type' ] ) );
  }
  
  if ( simple_query ( $q ) )
    {
	$mail = init_smarty ( );
	$mail -> template_dir = TEMPLATE_MAIL_DIR;
	$mail -> assign ( 'petition_url', PETITION_URL );
	$mail -> assign ( 'petition_name', lang('PETITION_NAME') );
	$mail -> assign ( 'firstname', $args [ 'firstname' ] );
	$mail -> assign ( 'key', $key );
	$content = $mail -> fetch ( 'petition-signature.tpl' );

	if ( gpt_send_mail ( mb_encode_mimeheader ( $args['firstname'] . ' ' . $args['name'] ) . ' <' . $args [ 'email' ] . '>', 
			     lang('Signature confirmation for the %s declaration', lang('PETITION_NAME')),
			     $content ) )
	{
	    $smarty -> assign ( 'email', $args [ 'email' ] );
	    $smarty -> display ( 'confirm.tpl' );
	}
	else
	{
	    croak ( "Mail error" );
	}
    }
  else
    {
	croak ( "Error" );
    }
}


function petition_generate_token ( $args ) 
{
    if ( $args [ 'signature_type' ] == 1 )
	return md5 ( sprintf ( "%d%s%s%s", rand(), 
			       $args [ 'organization_name' ], $args [ 'organization_website' ], 
			       $args [ 'email' ] ) );
    else
	return md5 ( sprintf ( "%d%s%s%s", rand(), 
			       $args [ 'firstname' ], $args [ 'name' ], 
			       $args [ 'email' ] ) );
}


function petition_confirm ( $key ) 
{
    global $smarty;

    if ( $key && strpos ( $key, '@' ) )
    {
	$signature = simple_unique_query ( sprintf ( "SELECT * FROM signature WHERE email = '%s'; ",
						     maybe_addslashes ( $key ) ) );
	if ( $signature )
	{
	    $mail = init_smarty ( );
	    $mail -> template_dir = TEMPLATE_MAIL_DIR;
	    $mail -> assign ( 'petition_url', PETITION_URL );
	    $mail -> assign ( 'petition_name', lang('PETITION_NAME') );
	    $mail -> assign ( 'firstname', $signature [ 'firstname' ] );
	    $mail -> assign ( 'key', $signature [ 'key' ] );
	    $content = $mail -> fetch ( 'petition-signature.tpl' );

	    if ( gpt_send_mail ( mb_encode_mimeheader ( $signature['firstname'] . ' ' . $signature['name'] ) . ' <' . $signature [ 'email' ] . '>',
				 lang('Signature confirmation for the %s declaration', lang('PETITION_NAME')),
				 $content ) )
	    {
		$smarty -> assign ( 'email', $signature [ 'email' ] );
		return $smarty -> display ( 'confirm.tpl' );
	    }
	    else
	    {
		croak ( "Mail error" );
	    }
	}
    }

    $signature = simple_unique_query ( sprintf ( "SELECT * FROM signature WHERE `key` = '%s';", 
						 maybe_addslashes ( $key ) ) );
    if ( $key && $signature )
    {
        $smarty -> assign ( 'signature', $signature );
	if  ( simple_query ( sprintf ( "UPDATE signature SET validated_time = NOW() WHERE `key` = '%s';", 
				       maybe_addslashes ( $key ) ) ) )
	    $smarty -> display ( 'validate.tpl' );
	else
	    croak ( lang ( 'Unable to validate your signature!' ) );
    }
    else
    {
	croak ( lang ( "Unable to find the signature and thus to validate it." ) );
    }
}


function petition_cancel ( $key )
{
    global $smarty;
    if ( is_null ( $key ) ) 
    {
	if ( array_key_exists ( 'email', $_POST ) )
	{
	    $signature = simple_unique_query ( sprintf ( "SELECT * " .
							 "  FROM signature " .
							 " WHERE `email` = '%s'; ",
							 maybe_addslashes ( $_POST [ 'email' ] ) ) );
	    if ( $signature )
	    {
		$mail = init_smarty ( );
		$mail -> template_dir = TEMPLATE_MAIL_DIR;
		$mail -> assign ( 'petition_url', PETITION_URL );
		$mail -> assign ( 'petition_name', lang('PETITION_NAME') );
		$mail -> assign ( 'firstname', $signature [ 'firstname' ] );
		$key = petition_generate_token ( $signature );
		$mail -> assign ( 'key', $key );
		simple_unique_query ( sprintf ( "UPDATE signature" .
						"   SET `key` = '%s' " .
						" WHERE email = '%s'; ",
						$key, maybe_addslashes ( $_POST [ 'email' ] ) ) );

		$content = $mail -> fetch ( 'petition-cancel.tpl' );

		if ( gpt_send_mail ( mb_encode_mimeheader ( $signature['firstname'] . ' ' . $signature['name'] ) . ' <' . $signature [ 'email' ] . '>',
				     lang('Signature cancellation for the %s declaration', lang('PETITION_NAME')),
				     $content ) )
		    $smarty -> assign ( 'email', $signature [ 'email' ] );
		else
		  {
		  print_r(error_get_last());
		  croak ( lang("Unable to send mail to email <tt>%s</tt>", $signature [ 'email' ] ) . "<br/>" . posix_strerror(posix_get_last_error()));
		  }
	    }
	    $smarty -> display ( 'cancel-confirm.tpl' );
	}
	else
	{
	    $smarty -> display ( 'cancel.tpl' );
	}
    }
    else
    {
	$signature = simple_unique_query ( sprintf ( "SELECT * " .
						     "  FROM signature " .
						     " WHERE `key` = '%s'; ",
						     maybe_addslashes ( $key ) ) );
	if ( $signature )
	{
	    simple_query ( sprintf ( "DELETE FROM signature " .
				     "      WHERE `signature_id` = '%d'; ",
				     $signature [ 'signature_id' ] ) );
	    $smarty -> display ( 'cancel-validate.tpl' );
	}
	else
	{
	    croak ( lang ( 'Unable to cancel signature, wrong key.' ) );
	}
    }
}



function petition_visibility ( $key )
{
    global $smarty;
    if ( is_null ( $key ) ) 
    {
	if ( array_key_exists ( 'email', $_POST ) )
	{
	    $signature = simple_unique_query ( sprintf ( "SELECT * " .
							 "  FROM signature " .
							 " WHERE `email` = '%s'; ",
							 maybe_addslashes ( $_POST [ 'email' ] ) ) );
	    if ( $signature )
	    {
		$mail = init_smarty ( );
		$mail -> template_dir = TEMPLATE_MAIL_DIR;
		$mail -> assign ( 'petition_url', PETITION_URL );
		$mail -> assign ( 'petition_name', lang('PETITION_NAME') );
		$mail -> assign ( 'firstname', $signature [ 'firstname' ] );
		$key = petition_generate_token ( $signature );
		$mail -> assign ( 'key', $key );
		simple_unique_query ( sprintf ( "UPDATE signature" .
						"   SET `key` = '%s' " .
						" WHERE email = '%s'; ",
						$key, maybe_addslashes ( $_POST [ 'email' ] ) ) );

		$content = $mail -> fetch ( 'petition-visibility.tpl' );

		if ( gpt_send_mail ( mb_encode_mimeheader ( $signature['firstname'] . ' ' . $signature['name'] ) . ' <' . $signature [ 'email' ] . '>',
				     lang('Signature confirmation for the %s declaration', lang('PETITION_NAME')),
				     $content ) )
		    $smarty -> assign ( 'email', $signature [ 'email' ] );
		else
		  croak ( lang("Unable to send mail to email <tt>%s</tt>", $signature [ 'email' ] ) . "<br/>" . posix_strerror(posix_get_last_error()));
	    }
	    $smarty -> display ( 'visibility-confirm.tpl' );
	}
	else
	{
	    $smarty -> display ( 'visibility.tpl' );
	}
    }
    else
    {
	if ( array_key_exists ( 'visibility', $_POST ) )
	{
	    if ( simple_unique_query ( sprintf ( "UPDATE signature " .
						 "   SET show_signature = '%d' " .
						 " WHERE `key` = '%s'; ",
						 $_POST [ 'visibility' ],
						 maybe_addslashes ( $key ) ) ) )
	    {
		$smarty -> assign ( 'notice', lang ( 'Visibility changed' ) );
	    }
	}

	$signature = simple_unique_query ( sprintf ( "SELECT * " .
						     "  FROM signature " .
						     " WHERE `key` = '%s'; ",
						     maybe_addslashes ( $key ) ) );

	if ( $signature )
	{
	    $smarty -> assign ( 'signature', $signature );
	    $smarty -> display ( 'visibility-validate.tpl' );
	}
	else
	{
	    croak ( lang ( 'Unable to change visibility for signature, wrong key.' ) );
	}
    }
}


function petition_show_page ( $page )
{
    if ( ! preg_match ( '/^[a-zA-Z0-9_\.-]+$/', $page ) )
	return croak ( 'Invalid page name' );

    $content = file_get_contents ( "pages/$page.org" );
    if ( ! $content )
	return croak ( 'Invalid page !' );

    global $smarty;
    $orgile = new orgile();
    $smarty -> assign ( 'page_content', $orgile->orgileThis( $content ) );
    $smarty -> display ( 'page.tpl' );
}

?>
