<?php // -*- c++ -*-

/* ************************************************************************** */
/*                                                                            */
/*     Copyright (C)	2010 Benjamin Drieu (bdrieu@april.org)		      */
/*                                                                            */
/*  This program is free software; you can redistribute it and/or modify      */
/*  it under the terms of the GNU General Public License as published by      */
/*  the Free Software Foundation; either version 2 of the License, or         */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU General Public License for more details.                              */
/*                                                                            */
/*  You should have received a copy of the GNU General Public License         */
/*  along with this program; if not, write to the Free Software               */
/*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */
/*                                                                            */
/* ************************************************************************** */

// Top directory where GSpeakUp is installed.  Do not modify
// TEMPLATE_DIR, TEMPLATE_MAIL_DIR or COMPILE_DIR unless needed.
define ( 'GPT_DIR', '/var/www/gsSpeakUp/' );
define ( 'TEMPLATE_DIR', GPT_DIR . '/templates/smarty/' );
define ( 'TEMPLATE_MAIL_DIR', GPT_DIR . '/templates/mail' );
define ( 'COMPILE_DIR', GPT_DIR . '/templates/compiled' );

// Domain where GSpeakUp runs.  Used for cookies.
define ( 'GPT_DOMAIN', 'gSpeakUp.org' );

// Top URL of your GSpeakUp instance.
define ( 'GPT_BASE_URL', 'http://gSpeakUp.org/' );

// Who sends the mails
define ( 'GPT_MAIL_BOT', 'Mail Bot <someone@somewhere.org>' );

// Email of the webmaster to send error reports to.
define ( 'GPT_WEBMASTER', 'admin@somewhere.org' );

// GPT theme, must be 'sandstone', 'flatty' or 'cerulean'
define ( 'GPT_THEME', 'sandstone' );

// GPT map, currently only 'europe_iso' and 'world' are supported.
define ( 'PETITION_MAP', 'world' );

// Goal to achieve
define ( 'PETITION_GOAL', 150 );

// MySQL parameters.
$host = 'localhost';
$database = 'petition';
$user = 'xxx';
$passwd = 'xxx';


// Include Smarty.  Change this if you do not want to use the Smarty
// shipped by GPT.
require GPT_DIR . '/libs/Smarty.class.php';

?>
