* About this campaign

This test campaign is not a campaign from [[http://www.april.org/][April]], but you can still
give them money.  ;-)

** Legal mentions

Blah

** About this site

This website is powered by [[http://gna.org/projects/gspeakup][gSpeakUp]], yeah!

<img src=http://wave.webaim.org/img/toolbar/wavesmalllogo.png alt=WAVE>
Accessibility validated by [[http://wave.webaim.org/refer][validée par WAVE]].

<img src=http://www.w3.org/html/logo/downloads/HTML5_Logo_64.png alt=HTML5>
This website is HTML5 validated.

** Licences

The /kitten.jpeg/ image is under the CC Attribution-ShareAlike 2.0 Generic license et and is from [[https://secure.flickr.com/photos/lachlanrogers/][Lachlan Rogers]]

Flags are taken from [[https://github.com/lafeber/world-flags-sprite/][world-flag-sprite]] package and are from Martijn
Lafeber, availabled under the following license:

#+BEGIN_EXAMPLE
Copyright (c) 2012 Martijn Lafeber

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#+END_EXAMPLE
