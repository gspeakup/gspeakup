<?php /* Smarty version 2.6.20, created on 2015-12-05 15:03:19
         compiled from validate.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'lang', 'validate.tpl', 3, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array('title' => 'PETITION_TITLE','no_control' => '1')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<h1><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Thanks for your signature and your support!<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></h1>

<p><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Adding your name to the list means that we show to the world how many of us there are. Don't hesitate to spread the word!<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>  
<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Do not hesitate to ask your friends and relative to sign this declaration or to share your signature using social networks.<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
</p>

<h2>Inscrivez vous à la liste de discussion <em>educ@april.org</em></h2>

<p>Cette liste de travail de l'April sert à coordonner les discussions
  autour du logiciel libre dans l'éducation.  Après confirmation de
  votre demande d'inscription, vous recevrez les mails à destination
  de <em>educ@april.org</em> et vous pourrez poster sur cette liste de
  discussion.</p>

<form class="form-horizontal" action="https://listes.april.org/wws" target="_blank" method="POST">
  <div class="input-group">
    <div class="col-sm-9">
      <input type="email" class="form-control" value="<?php echo $this->_tpl_vars['signature']['email']; ?>
" placeholder="Mon courriel" name="email"></div>
    <div class="col-sm-3">
    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;S'inscrire</button>
    </div>
  </div>
  <input type="hidden" name="list" value="educ" />
  <input type="hidden" name="action" value="subrequest" />
  <input type="hidden" name="action_subrequest" value="valider" />
</form>


<h2>Inscrivez vous à la lettre d'information mensuelle de l'April (liste april-actu@)</h2>

<p>Cette liste de diffusion est utilisée par l'April pour envoyer sa lettre d'information mensuelle ainsi que quelques rares annonces liées à l'association. Cette liste est utilisée pour communiquer vers l'extérieur et est modérée (seules quelques personnes peuvent poster).</p> 

<form class="form-horizontal" action="https://listes.april.org/wws" target="_blank" method="POST">
  <div class="input-group">
    <div class="col-sm-9">
      <input type="email" class="form-control" value="<?php echo $this->_tpl_vars['signature']['email']; ?>
" placeholder="Mon courriel" name="email"></div>
    <div class="col-sm-3">
    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;S'inscrire</button>
    </div>
  </div>
  <input type="hidden" name="list" value="april-actu" />
  <input type="hidden" name="action" value="subrequest" />
  <input type="hidden" name="action_subrequest" value="valider" />
</form>

<h2><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Share your signature!<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></h2>

<?php ob_start(); ?>
<?php $this->_tag_stack[] = array('lang', array('var' => $this->_tpl_vars['gpt_base_url'])); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>J'ai signé l'appel pour l'interopérabilité dans l'Éducation nationale de @aprilorg Et vous ? %s #InteropEduc<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
<?php $this->_smarty_vars['capture']['text'] = ob_get_contents(); ob_end_clean(); ?>

<div class="col-md-6">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <div class="panel-title" style="font-size:x-large"><img src="/images/identica.png" alt="Pump.io" />&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Share on identi.ca/pump.io<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></div>
    </div>

    <div class="panel-body">
      <p><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Text suggestion<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>:</p>
      <form action="http://identi.ca/">
        <textarea class="form-control" ><?php echo $this->_smarty_vars['capture']['text']; ?>
</textarea>
	<input class="btn btn-primary" type="submit" value="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Share!<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>">
      </form>
    </div>
  </div>
</div>

<div class="col-md-6">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <div class="panel-title" style="font-size:x-large"><img src="/images/tcheep.png" alt="Twitter" />&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Share on twitter<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></div>
    </div>
    
    <div class="panel-body">
      <p><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>We nevertheless discourage the use of non-free Twitter in favor of the use of Pump.io. <a href='https://www.fsf.org/twitter'>Read more</a> about the issues regarding Twitter on the Free Software Foundation website.<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>
      <form method="GET" action="https://twitter.com/intent/tweet">
	<textarea class="form-control" name="text"><?php echo $this->_smarty_vars['capture']['text']; ?>
</textarea>
	<input class="btn btn-primary" type="submit" value="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Share!<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>" />
      </form>
    </div>
  </div>
</div>

<p style="clear:both"><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Return to declaration page<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></p>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>