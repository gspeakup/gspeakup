<?php /* Smarty version 2.6.20, created on 2015-12-06 00:23:28
         compiled from footer.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'lang', 'footer.tpl', 19, false),)), $this); ?>

	</div> <!-- container -->
	</div> <!-- container -->
      </div>   <!-- content-wrapper -->
      
      <!-- Start Footer Wrapper -->

      <!-- End Footer Wrapper -->

    </div>   <!-- page -->

    <footer style="margin-bottom: 0px;">
      <div class="footer navbar navbar-default" style="margin-bottom: 0px;">
	<div class="container-fluid" style="margin-bottom: 0px;">
	  <div class="collapse navbar-collapse navbar-right">
	    <ul class="nav navbar-nav">
	      <li><a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/pages/About">About</a></li>
	      <li class="dropdown visible-xs">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>My signature<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>&nbsp;<span class="caret"></span></a>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/visibility"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Change signature visibility<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></li>
		  <li><a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Cancel my signature<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></li>
		</ul>
	      </li>
              <li><a href="http://gna.org/projects/gspeakup">Powered by gSpeakUp</a></li>
	    </ul>
	  </div>
	</div>
      </div>
    </footer>
  </body>
</html>