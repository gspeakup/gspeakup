<?php /* Smarty version 2.6.20, created on 2015-12-06 00:23:28
         compiled from header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'lang', 'header.tpl', 8, false),)), $this); ?>
<!DOCTYPE html>
<!--[if lte IE 7]> <html class="ie67 ie678" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="ie8 ie678" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]> <!--><html lang="en"> <!--<![endif]-->
  <head>
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <title><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?><?php echo $this->_tpl_vars['title']; ?>
<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/chosen.min.css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/petition.css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/jquery-ui.css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/bootstrap-theme.<?php echo $this->_tpl_vars['gpt_theme']; ?>
.min.css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/css/flags16.css" />
<?php if ($this->_tpl_vars['petition_map']): ?>
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jqvmap/jqvmap.css" />
<?php endif; ?>
<!--[if IE]>
<?php echo '
<style type="text/css">
.container, .container-fluid
{
    display:table;
    width: 100%;
}
.row
{
    display: table-row;
}
.col-sm-4, .col-sm-6, .col-md-4, .col-md-6
{
    display: table-cell;
}
</style>
'; ?>

<![endif]-->

    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/chosen.jquery.min.js"></script>
<?php if ($this->_tpl_vars['petition_map']): ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jqvmap/jquery.vmap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jqvmap/maps/jquery.vmap.<?php echo $this->_tpl_vars['petition_map']; ?>
.js"></script>
<?php endif; ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/jquery-ui-1.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/js/bootstrap.min.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>

  <body role="document" id="<?php echo $this->_tpl_vars['page']; ?>
">

    <nav class="navbar navbar-default navbar-default-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
	  <a class="navbar-brand" href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>PETITION_BRAND<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a>
        </div>
        <div class="navbar-collapse navbar-right hidden-xs">
          <ul class="nav navbar-nav">
	    <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-globe"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Language<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
	      <li><a href="?action=set_language&amp;language=en" hreflang="en" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>English<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>English<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></li>
	      <li><a href="?action=set_language&amp;language=fr" hreflang="fr" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>French<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>French<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></li>
	      <li><a href="?action=set_language&amp;language=lv" hreflang="lv" title="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Latvian<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Latvian<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></li>
	      </ul>
	    </li>
	    <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>My signature<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
		<li><a href="visibility"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Change signature visibility<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></li>
		<li><a href="cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Cancel my signature<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></li>
	      </ul>
	    </li>
	    <li class="dropdown visible-xs">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown">S'inscrire à la lettre mensuelle de l'April&nbsp;<span class="caret"></span></a>
	      <ul class="dropdown-menu navbar-default navbar-static-top" role="menu">
		<li>
		  <form class="navbar-form" action="https://listes.april.org/wws" method="POST">
		    <div class="input-group">
		      <input type="email" class="form-control"
			     placeholder="Mon courriel" name="email">
		      <div class="input-group-btn">
			<button class="btn btn-success"
				type="submit"><i class="glyphicon
							glyphicon-ok"></i>&nbsp;&nbsp;S'inscrire</button>
		      </div>
		    </div>
		    <input type="hidden" name="list" value="april-actu" />
		    <input type="hidden" name="action" value="subrequest" />
		    <input type="hidden" name="action_subrequest" value="valider" />
		  </form>
		</li>
	      </ul>
	    </li>
	    <li class="visible-xs"><a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/page/A-propos">À propos</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
 
    <div id="page">

      <div id="content-wrapper" class="wrapper" >
	<div class="container" role="main">	
	    
	    <?php if ($this->_tpl_vars['message']): ?>
	    <div class="alert alert-info"><?php echo $this->_tpl_vars['message']; ?>
</div>
	    <?php endif; ?>
	    <?php if ($this->_tpl_vars['notice']): ?>
	    <div class="alert alert-info"><?php echo $this->_tpl_vars['notice']; ?>
</div>
	    <?php endif; ?>
	    <?php if ($this->_tpl_vars['warning']): ?>
	    <div class="alert alert-warning"><?php echo $this->_tpl_vars['warning']; ?>
</div>
	    <?php endif; ?>
	    <?php if ($this->_tpl_vars['error']): ?>
	    <div class="alert alert-danger"><?php echo $this->_tpl_vars['error']; ?>
</div>
	    <?php endif; ?>

	    <div class="post">