<?php /* Smarty version 2.6.20, created on 2014-11-05 13:21:04
         compiled from admin/search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'admin/search.tpl', 40, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      Search results
    </h1>
    <ol class="breadcrumb">
      <li>
        <i class="glyphicon glyphicon-dashboard"></i>  <a href="/admin/">Dashboard</a>
      </li>
      <li class="active">
        <i class="glyphicon glyphicon-search"></i> Search results
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <?php if ($this->_tpl_vars['results']): ?>
    <table class="table table-bordered table-hover">
      <thead>
	<tr>
	  <th>Firstname</th>
	  <th>Name</th>
	  <th>Occupation</th>
	  <th>Type</th>
	  <th>Date</th>
	  <th>Ack</th>
	  <th>Actions</th>
      </thead>
      <tbody>
	<?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['results']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['step'] = 1;
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = $this->_sections['r']['loop'];
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
	<tr>
	  <td><?php echo $this->_tpl_vars['results'][$this->_sections['r']['index']]['firstname']; ?>
</td>
	  <td><?php echo $this->_tpl_vars['results'][$this->_sections['r']['index']]['name']; ?>
</td>
	  <td><?php echo $this->_tpl_vars['results'][$this->_sections['r']['index']]['occupation']; ?>
</td>
	  <td><?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['signature_type'] == 0): ?>Indiv.<?php else: ?>Orga<?php endif; ?></td>
	  <td><?php echo ((is_array($_tmp=$this->_tpl_vars['results'][$this->_sections['r']['index']]['signed_time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%d/%m/%Y %H:%M') : smarty_modifier_date_format($_tmp, '%d/%m/%Y %H:%M')); ?>
</td>
	  <td><?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['validated_time']): ?><i class="glyphicon glyphicon-ok-sign text-success"><span class="hidden">Yes</span></i><?php else: ?><i class="glyphicon glyphicon-question-sign text-warning"><span class="hidden">No</span></i><?php endif; ?></td>
	  <td><a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/admin/cancel/<?php echo $this->_tpl_vars['results'][$this->_sections['r']['index']]['signature_id']; ?>
" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a></td>
	  <?php endfor; endif; ?>
    </table>
    <?php endif; ?>
  </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>