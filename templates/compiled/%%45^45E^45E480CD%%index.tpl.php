<?php /* Smarty version 2.6.20, created on 2015-12-06 00:23:28
         compiled from index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'lang', 'index.tpl', 4, false),array('modifier', 'lang', 'index.tpl', 88, false),array('modifier', 'escape', 'index.tpl', 127, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array('title' => 'PETITION_TITLE','page' => 'index')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <div class="col-md-6 main-cols main-block">
    <h2 style="margin-top:0px"><span class="glyphicon glyphicon-question-sign"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Why should I sign?<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></h2>
    <p><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>PETITION_ARGUMENT<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>
  </div>

  <div class="col-md-6 main-cols main-block">
    <form class="" id="enter-mail" action="<?php echo $this->_tpl_vars['petition_url']; ?>
sign" method="POST">
      <div class="panel panel-primary">
	<div class="panel-heading">
	  <div class="panel-title" style="font-size:x-large;margin:0px;text-align:left;"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>PETITION_HEADER<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></div>
	</div>
	<div class="panel-body">
	  <p><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>PETITION_BODY<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
	    <label class="sr-only" for="email"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Enter email<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></label>
	  </p>

	  <input type="hidden" name="email_set" value="1">
	  <div class="col-md-7">
	    <input type="email" class="form-control" id="email" name="email" placeholder="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Email<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>" />
	  </div>
	  <div class="col-md-5">
	    <input type="submit" class="btn btn-primary" value="<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Validate my signature<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>" />
	  </div>
	</div>
      </div>
    </form>
  </div>
  
<?php if ($this->_tpl_vars['petition_goal']): ?>
  <div class="col-md-6 main-cols main-block" id="goal">
    <div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $this->_tpl_vars['petition_percentage']; ?>
"
	   aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $this->_tpl_vars['petition_percentage']; ?>
%;"><?php echo $this->_tpl_vars['petition_percentage']; ?>
%
      </div>
    </div>
    <?php $this->_tag_stack[] = array('lang', array('var' => ($this->_tpl_vars['signatories']).",".($this->_tpl_vars['petition_goal']))); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>%s signatures out of a %d goal<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
  </div>
<?php endif; ?>

    <div class="col-xs-12 col-md-6 pull-right main-cols main-block" id="signature-list">
      <h3><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Latest signatories<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></h3>
      <ul class="container-fluid">
	<?php unset($this->_sections['sign']);
$this->_sections['sign']['name'] = 'sign';
$this->_sections['sign']['loop'] = is_array($_loop=$this->_tpl_vars['latest_signatories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sign']['show'] = true;
$this->_sections['sign']['max'] = $this->_sections['sign']['loop'];
$this->_sections['sign']['step'] = 1;
$this->_sections['sign']['start'] = $this->_sections['sign']['step'] > 0 ? 0 : $this->_sections['sign']['loop']-1;
if ($this->_sections['sign']['show']) {
    $this->_sections['sign']['total'] = $this->_sections['sign']['loop'];
    if ($this->_sections['sign']['total'] == 0)
        $this->_sections['sign']['show'] = false;
} else
    $this->_sections['sign']['total'] = 0;
if ($this->_sections['sign']['show']):

            for ($this->_sections['sign']['index'] = $this->_sections['sign']['start'], $this->_sections['sign']['iteration'] = 1;
                 $this->_sections['sign']['iteration'] <= $this->_sections['sign']['total'];
                 $this->_sections['sign']['index'] += $this->_sections['sign']['step'], $this->_sections['sign']['iteration']++):
$this->_sections['sign']['rownum'] = $this->_sections['sign']['iteration'];
$this->_sections['sign']['index_prev'] = $this->_sections['sign']['index'] - $this->_sections['sign']['step'];
$this->_sections['sign']['index_next'] = $this->_sections['sign']['index'] + $this->_sections['sign']['step'];
$this->_sections['sign']['first']      = ($this->_sections['sign']['iteration'] == 1);
$this->_sections['sign']['last']       = ($this->_sections['sign']['iteration'] == $this->_sections['sign']['total']);
?>
	<li class="row"><div class="col-md-4 col-sm-2 col-xs-11"><?php 
	  $time_ago = explode ( ':',
	$this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['time_ago']);
	if ( $time_ago [ 0 ] >= 48 )
	{
	print strftime(lang('%%Y-%%m-%%d'),$this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['epoch']);
	}
	else if ( $time_ago [ 0 ] >= 24 )
	{
	print ( lang ( 'Yesterday' ) );
	}
	else if ( $time_ago [ 0 ] > 1 )
	{
	print ( lang ( "%d hours ago", $time_ago [ 0 ] ) );
	}
	else if ( $time_ago [ 0 ] >= 1 )
	{
	print ( lang ( "1 hour ago" ) );
	}
	else if ( $time_ago [ 1 ] >= 1 )
	{
	print ( lang ( "%d minutes ago", $time_ago [ 1 ] ) );
	}
	else
	{
	print ( lang ( "%d seconds ago", $time_ago [ 2 ] ) );
	}
	 ?>
	</div>
	  <div class="col-md-1 col-sm-1 col-xs-1">
	    <?php if ($this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['country']): ?>
	    <img width="32" height="32" src="<?php echo $this->_tpl_vars['petition_url']; ?>
images/blank.png" class="flag flag-<?php echo $this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['country']; ?>
" alt="Country" /> 
	    <?php else: ?>
	    <img src="<?php echo $this->_tpl_vars['petition_url']; ?>
images/unknown.png" alt="Country" /> 
	    <?php endif; ?>
	  </div>

	  <div class="col-md-7 col-sm-9 col-xs-10">
	<?php $this->assign('country', $this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['country']); ?>
	<?php if ($this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['show_signature']): ?>
	<?php if ($this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['signature_type'] == 1): ?>
	<u><?php echo $this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['organization_name']; ?>
</u><?php else: ?>
	<span data-toggle="tooltip" title="<?php echo $this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['occupation']; ?>
"><?php echo $this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['firstname']; ?>
 <?php echo $this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['name']; ?>
</span><?php endif; ?><?php if ($this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['country']): ?>, <em><?php echo ((is_array($_tmp=$this->_tpl_vars['country'])) ? $this->_run_mod_handler('lang', true, $_tmp) : lang($_tmp)); ?>
</em><?php endif; ?>
	<?php else: ?>
	<?php if ($this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['country']): ?><?php $this->_tag_stack[] = array('lang', array('var' => ((is_array($_tmp=$this->_tpl_vars['country'])) ? $this->_run_mod_handler('lang', true, $_tmp) : lang($_tmp)))); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>A signatory from <em>%s</em><?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?><?php else: ?><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>A signatory<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?><?php endif; ?><?php endif; ?>
	  </div>	
	</li>
      <?php endfor; endif; ?>
	<li><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
signatures"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>See all signatures<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></li>
      </ul>
    </div>
  </div>

<?php if ($this->_tpl_vars['petition_map']): ?>
  <div class="col-xs-12 col-md-6 pull-left main-cols main-block" id="welcome">
    <h2><span class="glyphicon glyphicon-globe"></span>&nbsp;<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Signatories map<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></h2>
<!--[if IE]>
<div class="alert alert-warning">
Map is not loading because you are using a joke web-browser (that is, 
Internet Explorer).  Please get some self-esteem and use a web browser
that does not suck.  Really.
</div>
<style type="text/css">
#vmap {
  background: #ccc !important;
}
</style>
<![endif]-->

      <div id="vmap" class="img-responsive" style="width: 100%; height: 400px;"><span>Map is loading ...</span></div>
    </div>
<?php endif; ?>



<?php if ($this->_tpl_vars['petition_map']): ?>
<script type="text/javascript">
var real_values = {};
jQuery(document).ready(function() {
    var country_values = {
<?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['by_country']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
'<?php echo $this->_tpl_vars['by_country'][$this->_sections['c']['index']]['code']; ?>
' : { 'id': '<?php echo $this->_tpl_vars['by_country'][$this->_sections['c']['index']]['code']; ?>
', 'name': '<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['by_country'][$this->_sections['c']['index']]['code'])) ? $this->_run_mod_handler('lang', true, $_tmp) : lang($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
', 'signatories': '<?php echo $this->_tpl_vars['by_country'][$this->_sections['c']['index']]['count']; ?>
', },
<?php endfor; endif; ?>
    };

    var orig = 
    {
<?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['all_countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
 '<?php echo $this->_tpl_vars['all_countries'][$this->_sections['c']['index']]['code']; ?>
' : { 'id': '<?php echo $this->_tpl_vars['all_countries'][$this->_sections['c']['index']]['code']; ?>
', 'name': '<?php echo ((is_array($_tmp=$this->_tpl_vars['all_countries'][$this->_sections['c']['index']]['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
', 'signatories': 0 },
<?php endfor; endif; ?>
    };

<?php echo '
function merge(set1, set2){
  for (var key in set2){
    if (set2.hasOwnProperty(key) && ! set1.hasOwnProperty(key))
      set1[key] = set2[key]
    real_values[key] = set1[key][\'signatories\'];
  }
  return set1
}
	merge ( country_values, orig );

    jQuery(\'#vmap\').vectorMap ({
        backgroundColor: \'#FFF\',
borderColor: \'#000\',
scaleColors: [\'#C8EEFF\', \'#006491\'],
normalizeFunction: \'polynomial\',
hoverColor: \'#2A69ac\',
'; ?>
map: '<?php echo $this->_tpl_vars['petition_map']; ?>
',<?php echo '
\'values\': real_values,
enableZoom: true,
showTooltip: true,
showTooltip: true,
onLabelShow: function(event, label, code)
{
    if ( country_values[code] )
            {
        var flag = \'<img width="24" src="'; ?>
<?php echo $this->_tpl_vars['petition_url']; ?>
<?php echo '/images/blank.png" class="flag flag-\'+country_values[code].id+\'"/> \';
if ( country_values[code].signatories>1) 
  var signatory_name = '; ?>
'<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>signatories<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>';<?php echo '
else
  var signatory_name = \''; ?>
<?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>signatory<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?><?php echo '\';
label.html(\'<h2 style="margin-top:0px;">\'+flag+country_values[code].name+\'</h2><p style="margin-top:0px;">\'+country_values[code].signatories+\' \' +signatory_name+\'</p>\');
    }
}
    });
    map = jQuery(\'#vmap\').data(\'mapObject\');
'; ?>

<?php if ($this->_tpl_vars['petition_map'] == 'europe_iso'): ?>
    map.scale = 0.85;
    map.transY = -73;
    map.transX = -50;
    map.applyTransform();
<?php endif; ?>
<?php echo '
});
$(\'#vmap span\').hide();
'; ?>

</script>
<?php endif; ?>

<script type="text/javascript">
<?php echo '
$(document).ready(function(){
$(\'span[data-toggle="tooltip"]\').tooltip();
});
'; ?>

</script>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>