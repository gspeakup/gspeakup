<?php /* Smarty version 2.6.20, created on 2015-10-02 18:59:23
         compiled from visibility-confirm.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'lang', 'visibility-confirm.tpl', 4, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array('title' => 'PETITION_TITLE','no_control' => '1')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['email']): ?>
<h1><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Signature visibility change confirmation<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></h1>

<p><?php $this->_tag_stack[] = array('lang', array('var' => ($this->_tpl_vars['email']))); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>A signature visibility change confirmation has been sent to email <tt>%s</tt>.<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>
<p><?php $this->_tag_stack[] = array('lang', array('var' => ($this->_tpl_vars['gpt_mail_bot']))); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>If you do not receive confirmation mail soon, please check your spam folder for a mail sent from <tt>%s</tt>.<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>
<?php else: ?>
<h1><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Signature visibility change<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></h1>
<div class="error"><p><?php $this->_tag_stack[] = array('lang', array('var' => ($this->_tpl_vars['email']))); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Unable to find email <tt>%s</tt> in database.<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>
<p><?php $this->_tag_stack[] = array('lang', array('var' => $this->_tpl_vars['gpt_mail_bot'])); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>If you feel this is an error, please do not hesitate to contact us by email at <i>%s</i>.<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p></div>
<?php endif; ?>

<p><a href="<?php echo $this->_tpl_vars['petition_url']; ?>
"><?php $this->_tag_stack[] = array('lang', array()); $_block_repeat=true;do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>Return to declaration page<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo do_translation($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></a></p>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>