<?php /* Smarty version 2.6.20, created on 2014-11-18 22:21:02
         compiled from show-confirm.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      Show signature
      <small>Confirmation</small>
    </h1>
    <ol class="breadcrumb">
      <li>
        <i class="glyphicon glyphicon-dashboard"></i>  <a href="/admin/">Dashboard</a>
      </li>
      <li class="active">
        <i class="glyphicon glyphicon-search"></i> Show signature
      </li>
    </ol>

    <p>Showing of signature of <b><?php echo $this->_tpl_vars['signature']['firstname']; ?>
 <?php echo $this->_tpl_vars['signature']['name']; ?>
</b> achieved !</p>
    <p><a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/admin/hide/<?php echo $this->_tpl_vars['signature']['signature_id']; ?>
" class="btn btn-warning">Hide signature</a></p>
  </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</body>

</html>