<?php /* Smarty version 2.6.20, created on 2015-01-05 11:49:22
         compiled from search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'search.tpl', 39, false),array('modifier', 'date_format', 'search.tpl', 49, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      <i class="glyphicon glyphicon-search"></i> Search <?php if ($this->_tpl_vars['query']): ?>for «&nbsp;<?php echo $this->_tpl_vars['query']; ?>
&nbsp;»<?php endif; ?>
      <small><?php echo $this->_tpl_vars['results_count']; ?>
 result<?php if ($this->_tpl_vars['results_count'] > 1): ?>s<?php endif; ?></small> 
    </h1>
    <ol class="breadcrumb">
      <li>
        <i class="glyphicon glyphicon-dashboard"></i>  <a href="/admin/">Dashboard</a>
      </li>
      <li class="active">
        <i class="glyphicon glyphicon-search"></i> Search results
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <?php if ($this->_tpl_vars['results']): ?>
    <table class="table table-bordered table-hover">
      <thead>
	<tr>
	  <th>Firstname</th>
	  <th>Name</th>
	  <th>Occupation</th>
	  <th>Email</th>
	  <th>Type</th>
	  <th class="date-euro">Date</th>
	  <th>Ack</th>
	  <th>Actions</th>
      </thead>
      <tbody>
	<?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['results']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['step'] = 1;
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = $this->_sections['r']['loop'];
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
	<tr>
<?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['signature_type'] == 0): ?>
	  <td><?php echo ((is_array($_tmp=$this->_tpl_vars['results'][$this->_sections['r']['index']]['firstname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</td>
	  <td><?php echo ((is_array($_tmp=$this->_tpl_vars['results'][$this->_sections['r']['index']]['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</td>
	  <td><?php echo ((is_array($_tmp=$this->_tpl_vars['results'][$this->_sections['r']['index']]['occupation'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</td>
<?php else: ?>
	  <td colspan="3"><?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['organization_website']): ?><a href="<?php echo $this->_tpl_vars['results'][$this->_sections['r']['index']]['organization_website']; ?>
"><?php endif; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['results'][$this->_sections['r']['index']]['organization_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['organization_website']): ?></a><?php endif; ?></td>
	  <td style="display:none !important;"></td>
	  <td style="display:none !important;"></td>
<?php endif; ?>
	  <td><?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['keep_mail']): ?><a href="mailto:<?php echo ((is_array($_tmp=$this->_tpl_vars['results'][$this->_sections['r']['index']]['email'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"><?php endif; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['results'][$this->_sections['r']['index']]['email'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['keep_mail']): ?></a><?php endif; ?></td>
	  <td><?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['signature_type'] == 0): ?>Indiv.<?php else: ?>Orga<?php endif; ?></td>
	  <td><?php echo ((is_array($_tmp=$this->_tpl_vars['results'][$this->_sections['r']['index']]['signed_time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%d/%m/%Y %H:%M') : smarty_modifier_date_format($_tmp, '%d/%m/%Y %H:%M')); ?>
</td>
	  <td><?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['validated_time']): ?><i class="glyphicon glyphicon-ok-sign text-success"><span class="hidden">Yes</span></i><?php else: ?><i class="glyphicon glyphicon-question-sign text-warning"><span class="hidden">No</span></i><?php endif; ?></td>
	  <td>
<a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/admin/cancel/<?php echo $this->_tpl_vars['results'][$this->_sections['r']['index']]['signature_id']; ?>
" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a> 
<?php if ($this->_tpl_vars['results'][$this->_sections['r']['index']]['show_signature']): ?>
	  <a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/admin/hide/<?php echo $this->_tpl_vars['results'][$this->_sections['r']['index']]['signature_id']; ?>
" class="btn btn-xs btn-warning"><span class="glyphicon glyphicon-eye-close"></span> Hide</a>
<?php else: ?>
	  <a href="<?php echo $this->_tpl_vars['gpt_base_url']; ?>
/admin/hide/<?php echo $this->_tpl_vars['results'][$this->_sections['r']['index']]['signature_id']; ?>
" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-eye-open"></span> Show</a>
<?php endif; ?>
</td>
	  <?php endfor; endif; ?>
    </table>
    <?php else: ?> 
    <p>No result found...</p>
    <?php endif; ?>
  </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script src="../js/jquery.dataTables.js"></script>
<script src="../js/dataTables.bootstrap.js"></script>
<script>
  <?php echo '
  $(document).ready(function() {
  $(\'table\').dataTable(
{
  aoColumnDefs: {
        target: \'date-eu\',
        sType: "date-eu"
    }
}
);


jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-euro-pre": function ( a ) {
        var x;
 
        if ( $.trim(a) !== \'\' ) {
            var frDatea = $.trim(a).split(\'\');
            var frTimea = frDatea[1].split(\':\');
            var frDatea2 = frDatea[0].split(\'/\');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
        }
        else {
            x = Infinity;
        }
 
        return x;
    },
 
    "date-euro-asc": function ( a, b ) {
        return a - b;
    },
 
    "date-euro-desc": function ( a, b ) {
        return b - a;
    }
} );


  });

  '; ?>

</script>


</body>

</html>