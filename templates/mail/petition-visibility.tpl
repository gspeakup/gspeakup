{lang var=`$firstname`}Dear %s{/lang},

{textformat style="mail"}{lang var=$petition_name}you receive this email as you asked to change visibility for your signature of the %s declaration.  We require that you confirm your request in to avoid false demands.{/lang}


{lang}To validate your request, click on the following link :{/lang}{/textformat}


  {$petition_url}visibility/{$key}


{textformat style="mail"}{lang}Note: some email programs would split the preceding URL in two lines.{/lang}{/textformat}
