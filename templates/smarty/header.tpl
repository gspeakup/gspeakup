<!DOCTYPE html>
<!--[if lte IE 7]> <html class="ie67 ie678" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="ie8 ie678" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]> <!--><html lang="en"> <!--<![endif]-->
  <head>
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <title>{lang}{$title}{/lang}</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{$gpt_base_url}/css/chosen.min.css" />
    <link rel="stylesheet" href="{$gpt_base_url}/css/petition.css" />
    <link rel="stylesheet" href="{$gpt_base_url}/css/jquery-ui.css" />
    <link rel="stylesheet" href="{$gpt_base_url}/css/bootstrap-theme.{$gpt_theme}.min.css" />
    <link rel="stylesheet" href="{$gpt_base_url}/css/flags16.css" />
{if $petition_map}
    <link rel="stylesheet" href="{$gpt_base_url}/js/jqvmap/jqvmap.css" />
{/if}
<!--[if IE]>
{literal}
<style type="text/css">
.container, .container-fluid
{
    display:table;
    width: 100%;
}
.row
{
    display: table-row;
}
.col-sm-4, .col-sm-6, .col-md-4, .col-md-6
{
    display: table-cell;
}
</style>
{/literal}
<![endif]-->

    <script type="text/javascript" src="{$gpt_base_url}/js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="{$gpt_base_url}/js/chosen.jquery.min.js"></script>
{if $petition_map}
    <script type="text/javascript" src="{$gpt_base_url}/js/jqvmap/jquery.vmap.min.js"></script>
    <script type="text/javascript" src="{$gpt_base_url}/js/jqvmap/maps/jquery.vmap.{$petition_map}.js"></script>
{/if}
    <script type="text/javascript" src="{$gpt_base_url}/js/jquery-ui-1.custom.min.js"></script>
    <script type="text/javascript" src="{$gpt_base_url}/js/bootstrap.min.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>

  <body role="document" id="{$page}">

    <nav class="navbar navbar-default navbar-default-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
	  <a class="navbar-brand" href="{$gpt_base_url}">{lang}PETITION_BRAND{/lang}</a>
        </div>
        <div class="navbar-collapse navbar-right hidden-xs">
          <ul class="nav navbar-nav">
	    <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-globe"></span>&nbsp;{lang}Language{/lang} <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
	      <li><a href="?action=set_language&amp;language=en" hreflang="en" title="{lang}English{/lang}">{lang}English{/lang}</a></li>
	      <li><a href="?action=set_language&amp;language=fr" hreflang="fr" title="{lang}French{/lang}">{lang}French{/lang}</a></li>
	      <li><a href="?action=set_language&amp;language=lv" hreflang="lv" title="{lang}Latvian{/lang}">{lang}Latvian{/lang}</a></li>
	      </ul>
	    </li>
	    <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-pencil"></span>&nbsp;{lang}My signature{/lang}&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
		<li><a href="visibility"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;{lang}Change signature visibility{/lang}</a></li>
		<li><a href="cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;{lang}Cancel my signature{/lang}</a></li>
	      </ul>
	    </li>
	    <li class="dropdown visible-xs">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown">S'inscrire à la lettre mensuelle de l'April&nbsp;<span class="caret"></span></a>
	      <ul class="dropdown-menu navbar-default navbar-static-top" role="menu">
		<li>
		  <form class="navbar-form" action="https://listes.april.org/wws" method="POST">
		    <div class="input-group">
		      <input type="email" class="form-control"
			     placeholder="Mon courriel" name="email">
		      <div class="input-group-btn">
			<button class="btn btn-success"
				type="submit"><i class="glyphicon
							glyphicon-ok"></i>&nbsp;&nbsp;S'inscrire</button>
		      </div>
		    </div>
		    <input type="hidden" name="list" value="april-actu" />
		    <input type="hidden" name="action" value="subrequest" />
		    <input type="hidden" name="action_subrequest" value="valider" />
		  </form>
		</li>
	      </ul>
	    </li>
	    <li class="visible-xs"><a href="{$gpt_base_url}/page/A-propos">À propos</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
 
    <div id="page">

      <div id="content-wrapper" class="wrapper" >
	<div class="container" role="main">	
	    
	    {if $message}
	    <div class="alert alert-info">{$message}</div>
	    {/if}
	    {if $notice}
	    <div class="alert alert-info">{$notice}</div>
	    {/if}
	    {if $warning}
	    <div class="alert alert-warning">{$warning}</div>
	    {/if}
	    {if $error}
	    <div class="alert alert-danger">{$error}</div>
	    {/if}

	    <div class="post">
