{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<h1>{lang}One last step to confirm your signature!{/lang}</h1>

<p>{lang}You have signed the declaration and we thank you a lot for your support!{/lang}</p>

<p>{lang var=$email}A confirmation mail has been sent to <tt>%s</tt>, please follow the confirmation link inside in order to confirm your signature.{/lang}</p>

<p>{lang var=$gpt_mail_bot}If you do not receive confirmation mail soon, please check your spam folder for a mail sent from <tt>%s</tt>.{/lang}</p>

<p><a href="{$petition_url}">{lang}Return to declaration page{/lang}</a></p>

{include file="footer.tpl"}
