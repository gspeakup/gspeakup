{include file="header.tpl" title="PETITION_TITLE" page="index"}

  <div class="col-md-6 main-cols main-block">
    <h2 style="margin-top:0px"><span class="glyphicon glyphicon-question-sign"></span>&nbsp;{lang}Why should I sign?{/lang}</h2>
    <p>{lang}PETITION_ARGUMENT{/lang}</p>
  </div>

  <div class="col-md-6 main-cols main-block">
    <form class="" id="enter-mail" action="{$petition_url}sign" method="POST">
      <div class="panel panel-primary">
	<div class="panel-heading">
	  <div class="panel-title" style="font-size:x-large;margin:0px;text-align:left;"><span class="glyphicon glyphicon-pencil"></span>&nbsp;{lang}PETITION_HEADER{/lang}</div>
	</div>
	<div class="panel-body">
	  <p>{lang}PETITION_BODY{/lang}
	    <label class="sr-only" for="email">{lang}Enter email{/lang}</label>
	  </p>

	  <input type="hidden" name="email_set" value="1">
	  <div class="col-md-7">
	    <input type="email" class="form-control" id="email" name="email" placeholder="{lang}Email{/lang}" />
	  </div>
	  <div class="col-md-5">
	    <input type="submit" class="btn btn-primary" value="{lang}Validate my signature{/lang}" />
	  </div>
	</div>
      </div>
    </form>
  </div>
  
{if $petition_goal}
  <div class="col-md-6 main-cols main-block" id="goal">
    <div class="progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="{$petition_percentage}"
	   aria-valuemin="0" aria-valuemax="100" style="width: {$petition_percentage}%;">{$petition_percentage}%
      </div>
    </div>
    {lang var=$signatories,$petition_goal}%s signatures out of a %d goal{/lang}
  </div>
{/if}

    <div class="col-xs-12 col-md-6 pull-right main-cols main-block" id="signature-list">
      <h3><span class="glyphicon glyphicon-pencil"></span>&nbsp;{lang}Latest signatories{/lang}</h3>
      <ul class="container-fluid">
	{section name=sign loop=$latest_signatories}
	<li class="row"><div class="col-md-4 col-sm-2 col-xs-11">{php}
	  $time_ago = explode ( ':',
	$this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['time_ago']);
	if ( $time_ago [ 0 ] >= 48 )
	{
	print strftime(lang('%%Y-%%m-%%d'),$this->_tpl_vars['latest_signatories'][$this->_sections['sign']['index']]['epoch']);
	}
	else if ( $time_ago [ 0 ] >= 24 )
	{
	print ( lang ( 'Yesterday' ) );
	}
	else if ( $time_ago [ 0 ] > 1 )
	{
	print ( lang ( "%d hours ago", $time_ago [ 0 ] ) );
	}
	else if ( $time_ago [ 0 ] >= 1 )
	{
	print ( lang ( "1 hour ago" ) );
	}
	else if ( $time_ago [ 1 ] >= 1 )
	{
	print ( lang ( "%d minutes ago", $time_ago [ 1 ] ) );
	}
	else
	{
	print ( lang ( "%d seconds ago", $time_ago [ 2 ] ) );
	}
	{/php}
	</div>
	  <div class="col-md-1 col-sm-1 col-xs-1">
	    {if $latest_signatories[sign].country}
	    <img width="32" height="32" src="{$petition_url}images/blank.png" class="flag flag-{$latest_signatories[sign].country}" alt="Country" /> 
	    {else}
	    <img src="{$petition_url}images/unknown.png" alt="Country" /> 
	    {/if}
	  </div>

	  <div class="col-md-7 col-sm-9 col-xs-10">
	{assign var=country value=$latest_signatories[sign].country}
	{if $latest_signatories[sign].show_signature}
	{if $latest_signatories[sign].signature_type == 1}
	<u>{$latest_signatories[sign].organization_name}</u>{else}
	<span data-toggle="tooltip" title="{$latest_signatories[sign].occupation}">{$latest_signatories[sign].firstname} {$latest_signatories[sign].name}</span>{/if}{if $latest_signatories[sign].country}, <em>{$country|lang}</em>{/if}
	{else}
	{if $latest_signatories[sign].country}{lang var=$country|lang}A signatory from <em>%s</em>{/lang}{else}{lang}A signatory{/lang}{/if}{/if}
	  </div>	
	</li>
      {/section}
	<li><a href="{$petition_url}signatures">{lang}See all signatures{/lang}</a></li>
      </ul>
    </div>
  </div>

{if $petition_map}
  <div class="col-xs-12 col-md-6 pull-left main-cols main-block" id="welcome">
    <h2><span class="glyphicon glyphicon-globe"></span>&nbsp;{lang}Signatories map{/lang}</h2>
<!--[if IE]>
<div class="alert alert-warning">
Map is not loading because you are using a joke web-browser (that is, 
Internet Explorer).  Please get some self-esteem and use a web browser
that does not suck.  Really.
</div>
<style type="text/css">
#vmap {ldelim}
  background: #ccc !important;
{rdelim}
</style>
<![endif]-->

      <div id="vmap" class="img-responsive" style="width: 100%; height: 400px;"><span>Map is loading ...</span></div>
    </div>
{/if}



{if $petition_map}
<script type="text/javascript">
var real_values = {ldelim}{rdelim};
jQuery(document).ready(function() {ldelim}
    var country_values = {ldelim}
{section name=c loop=$by_country}
'{$by_country[c].code}' : {ldelim} 'id': '{$by_country[c].code}', 'name': '{$by_country[c].code|lang|escape}', 'signatories': '{$by_country[c].count}', {rdelim},
{/section}
    {rdelim};

    var orig = 
    {ldelim}
{section name=c loop=$all_countries}
 '{$all_countries[c].code}' : {ldelim} 'id': '{$all_countries[c].code}', 'name': '{$all_countries[c].name|escape}', 'signatories': 0 {rdelim},
{/section}
    {rdelim};

{literal}
function merge(set1, set2){
  for (var key in set2){
    if (set2.hasOwnProperty(key) && ! set1.hasOwnProperty(key))
      set1[key] = set2[key]
    real_values[key] = set1[key]['signatories'];
  }
  return set1
}
	merge ( country_values, orig );

    jQuery('#vmap').vectorMap ({
        backgroundColor: '#FFF',
borderColor: '#000',
scaleColors: ['#C8EEFF', '#006491'],
normalizeFunction: 'polynomial',
hoverColor: '#2A69ac',
{/literal}map: '{$petition_map}',{literal}
'values': real_values,
enableZoom: true,
showTooltip: true,
showTooltip: true,
onLabelShow: function(event, label, code)
{
    if ( country_values[code] )
            {
        var flag = '<img width="24" src="{/literal}{$petition_url}{literal}/images/blank.png" class="flag flag-'+country_values[code].id+'"/> ';
if ( country_values[code].signatories>1) 
  var signatory_name = {/literal}'{lang}signatories{/lang}';{literal}
else
  var signatory_name = '{/literal}{lang}signatory{/lang}{literal}';
label.html('<h2 style="margin-top:0px;">'+flag+country_values[code].name+'</h2><p style="margin-top:0px;">'+country_values[code].signatories+' ' +signatory_name+'</p>');
    }
}
    });
    map = jQuery('#vmap').data('mapObject');
{/literal}
{if $petition_map == 'europe_iso'}
    map.scale = 0.85;
    map.transY = -73;
    map.transX = -50;
    map.applyTransform();
{/if}
{literal}
});
$('#vmap span').hide();
{/literal}
</script>
{/if}

<script type="text/javascript">
{literal}
$(document).ready(function(){
$('span[data-toggle="tooltip"]').tooltip();
});
{/literal}
</script>

{include file="footer.tpl"}
