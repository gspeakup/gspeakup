{include file="header.tpl"}

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</h1>

    <div class="col-sm-12">
      <div class="panel panel-default">
	<div class="panel-heading">
	<i class="glyphicon glyphicon-calendar"></i> By signature time
	</div>
	<div class="panel-body">
	  <div class="flot-chart" style="padding: 0px;">
            <div style="padding: 0px;" class="flot-chart-content" id="signatures"></div>
	  </div>
	</div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="panel panel-default">
	<div class="panel-heading">
	  <i class="glyphicon glyphicon-stats"></i> By signature type
	</div>
	<div class="panel-body">
	  <div class="flot-chart" style="padding: 0px;">
            <div style="padding: 0px;" class="flot-chart-content" id="signature-type-chart"></div>
	  </div>
	</div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="panel panel-default">
	<div class="panel-heading">
	  <i class="glyphicon glyphicon-globe"></i> By country
	</div>
	<div class="panel-body">
	  <div class="flot-chart">
            <div class="flot-chart-content" id="signature-country-chart"></div>
	  </div>
	</div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="panel panel-default">
	<div class="panel-heading">
	  <i class="glyphicon glyphicon-cloud-download"></i> By referer
	</div>
	<div class="panel-body">
	  {section name=r loop=$referers_domains}
	  <div class="list-group list-group-item">
	    <span class="badge alert-danger">{$referers_domains[r].count}</span>
	    <h4 class="list-group-item-heading">{$referers_domains[r].domain|escape}</h4>
	  {section name=r2 loop=$referers}
	  {if $referers[r2].domain == $referers_domains[r].domain}
	  </div>
	  <div class="list-group list-group-item"><span class="badge alert-warning">{$referers[r2].count}</span>
	    <p class="list-group-item-text">
	      <a href="{if $referers[r2].referer|strstr:'t.co'}https://twitter.com/search?q={$referers[r2].referer|escape:'url'}{else}{$referers[r2].referer|escape}{/if}">{$referers[r2].referer|escape}</a></p>
	  {/if}
	  {/section}
	  </div>
	  {/section}
	</div>
      </div>
    </div>

  </div>
</div>

{include file="footer.tpl"}

<script src="{$gpt_base_url}/js/jquery.flot.js"></script>
<script src="{$gpt_base_url}/js/jquery.flot.pie.js"></script>
<script src="{$gpt_base_url}/js/jquery.flot.resize.js"></script>
<script src="{$gpt_base_url}/js/jquery.flot.time.js"></script>
<script src="{$gpt_base_url}/js/jquery.flot.stack.js"></script>

<script>
  var type_data = [{ldelim}
{if $signature_type[0].count}
        label: "Individuals",
        data: {$signature_type[0].count|string_format:'%d'}
    {rdelim}{if $signature_type[1].count}, {ldelim}
        label: "Organizations",
        data: {$signature_type[1].count|string_format:'%d'}
    {rdelim}{/if}
{/if}];

  var country_data = [
{section name=c loop=$countries}
  {ldelim}label: '{$countries[c].country|lang} ({$countries[c].country}) : {$countries[c].count}',data:{$countries[c].count}{rdelim},
{/section}
];

{literal}
function labelFormatter(label, series) {
return "<div style=text-align:center; padding:2px; color:#000;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}
function country_labelFormatter(label, series) {
return "<div class=\"flag flag-"+label.match(/ \(([a-z]*)\) :/)[1]+"\" style=\"line-height: 2em;text-align:center; padding:2px; color:#000;\">" + "<br/>" + Math.round(series.percent) + "%</div>";
}

$(document).ready(function() {
$.plot('#signature-type-chart', type_data, {
    series: {
        pie: {
            innerRadius: 0.3,
            show: true,
	    label: {
                show: true,
                radius: 0.5,
                threshold: 0.1,
		formatter: labelFormatter
            }
        },
	shadowSize: 3,
    },
    legend: {
        show: true
    },
});

$.plot('#signature-country-chart', country_data, {
    series: {
        pie: {
            innerRadius: 0.3,
            show: true,
	    label: {
                show: true,
                radius: 0.5,
                threshold: 0.1,
		formatter: country_labelFormatter
            }
        },
	shadowSize: 3,
    },
    legend: {
        show: true
    }
});

var d1 = [{/literal}
{section name=s loop=$signatures}
[{$signatures[s].date}000, {$signatures[s].count}]{if !$smarty.section.s.last},{/if}
{/section}
{literal}
];

var total = 0;
var d2 = [{/literal}
{section name=s2 loop=$signatures_total}
[{$signatures_total[s2].date}000, {$signatures_total[s2].count}]{if !$smarty.section.s2.last},{/if}
{/section}
{literal}
];

var d3 = [];
for (var i = 0; i < d2.length; i += 1)
{
  total += d2 [i][1];
  d3[i] = [ d2[i][0], total ];
}


$.plot("#signatures", [ { label: 'Signatures', data: d1 }, { label: 'Total', data: d3 } ], 
{ 
series: {
  lines: { show: true, fill: true},
  lines: { show: true, fill: true},
},
  xaxis: {
    mode: "time",
    minTickSize: [1, "hour"],
    min: {/literal}{$signatures_stats.min_signature}000,
    max: {$signatures_stats.max_signature}000{literal},
  },
  legend: { position: "nw" },
  grid: { backgroundColor: { colors: [ "#fff", "#eee" ] } },
}

 );

});
{/literal}
</script>

</body>

</html>
