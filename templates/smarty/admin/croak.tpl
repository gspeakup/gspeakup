{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<h1 class="post-title text-danger">{lang}Error{/lang} !</h1>

{if $croak}
<div class="alert alert-danger"><p>{$croak}</p></div>
{/if}

{if $croak_notice}
<div class="alert alert-info">{$croak_notice}</div>
{/if}

<p><a href="javascript:history.go(-1);">Return</a></p>

{include file="footer.tpl"}

</body>

</html>
