<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>gSpeakUp admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="{$gpt_base_url}/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{$gpt_base_url}/css/sb-admin.css" rel="stylesheet">
    <link href="{$gpt_base_url}/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Fonts and flags -->
    <link href="{$gpt_base_url}/css/flags32.css" rel="stylesheet">
    <link href="{$gpt_base_url}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{$gpt_base_url}/admin/">gSpeakUp admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="{$gpt_base_url}/admin/" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{$gpt_base_url}/admin/logout"><i class="glyphicon glyphicon-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
		  <li class="sidebar-search">
		    <form class="input-group custom-search-form" style="padding:0px;margin:0px;" action="{$gpt_base_url}/admin/search" method="POST">
		      <input type="text" name="q" class="form-control" placeholder="Search...">
		      <span class="input-group-btn">
                        <button onclick="form.submit();" class="btn btn-default form-control" type="button">
                          <i class="glyphicon glyphicon-search"></i>
                        </button>
		      </span>
		    </form>
                    <!-- /input-group -->
                  </li>
                  <li>
                    <a href="{$gpt_base_url}/admin/"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</a>
                  </li>
                  <li>
                    <a href="{$gpt_base_url}/admin/search"><i class="glyphicon glyphicon-search"></i> Search</a>
                  </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">
