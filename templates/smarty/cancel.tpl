{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<form class="form-horizontal" role="form" id="cancel-mail" action="{$petition_url}/cancel" method="POST">
  <h1>{lang}Cancel my signature{/lang}</h1>
  <p>{lang var=`$petition_name`}You have signed the '<b>%s</b>' declaration and want to cancel your signature.{/lang}
  {lang}Please fill in your email address in order to authenticate your request.{/lang}</p>
  <div class="form-group">
    <div class="col-sm-5">
      <input type="email" class="form-control" name="email" placeholder="{lang}Enter email{/lang}" />
    </div>
    <input type="submit" class="btn btn-danger" value="{lang}Cancel my signature{/lang}"/>
  </div>
  
    ... {lang}or{/lang} <a href="{$petition_url}">{lang}return to declaration page{/lang}</a>
</form>

{include file="footer.tpl"}
