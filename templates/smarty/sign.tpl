{include file="header.tpl" title="PETITION_TITLE" no_control='1'}

<div>
  <h3>{lang}PETITION_HEADER{/lang}</h3>

  <p>{lang}PETITION_BODY{/lang}</p>
</div>

<div class="row">
  <div class="col-md-6">
    <form class="form-horizontal" id="sign-organization" action="{$petition_url}validate" method="POST" role="form">
      <div class="panel panel-primary">
	<div class="panel-heading">
	  <h3 class="panel-title">{lang}I represent an organization{/lang}</h3>
	</div>
	<div class="panel-body">
	  <input type="hidden" name="signature_type" value="1" />
	  <input type="hidden" name="email_set" value="{$email_set}" />
	  {if ! $email_set || ! $email }
	  <div class="form-group form-group-sm {if $signature_type == 1 && $errors.email}has-error{/if}">
	    <label for="email" class="col-sm-5 control-label">{lang}Email{/lang}</label>
	    <div class="col-sm-7">
	      <input class="form-control" type="text" 
		     name="email" 
		     placeholder="{lang}Email{/lang} ({lang}mandatory{/lang})" 
		     {if $email}value="{$email}"{/if} />
	    </div>
	  </div>
	  {else}
	  <input type="hidden" name="email" value="{$email}" />
	  {/if}
	  <input type="hidden" name="signature_type" value="1" />

	  <div class="form-group form-group-sm {if $signature_type == 1 && $errors.organization_name}has-error{/if}">
	    <label for="organization_name" class="col-sm-5 control-label">{lang}Organization name{/lang}</label>
	    <div class="col-sm-7">
	      <input class="form-control" type="text"
		     name="organization_name" 
		     placeholder="{lang}Organization name{/lang} ({lang}mandatory{/lang})" 
		     {if $organization_name}value="{$organization_name}"{/if} />
	    </div>
	  </div>

	  <div class="form-group form-group-sm {if $signature_type == 1 && $errors.organization_description}has-error{/if}">
	    <label for="organization_description" class="col-sm-5 control-label">{lang}Organization description{/lang}</label>
	    <div class="col-sm-7">
	      <input class="form-control" type="text"
		     name="organization_description" 
		     placeholder="{lang}Organization (very short) description{/lang}" 
		     {if $organization_description}value="{$organization_description}"{/if} />
	    </div>
	  </div>

	  <div class="form-group form-group-sm {if $signature_type == 1 && $errors.organization_website}has-error{/if}">
	    <label for="organization_website" class="col-sm-5 control-label">{lang}Website{/lang}</label>
	    <div class="col-sm-7">
	      <input class="form-control" type="text"
		     name="organization_website" 
		     placeholder="{lang}Organization (very short) description{/lang}" 
		     {if $organization_website}value="{$organization_website}"{/if} />
	    </div>
	  </div>

	  <div class="form-group form-group-sm {if $signature_type == 1 && $errors.country}has-error{/if}">
	    <label for="country" class="col-sm-5 control-label">{lang}Country{/lang}</label>
	    <div class="col-sm-7">
	      <select name="country" id="countries-1" data-placeholder="{lang}Choose your country{/lang} ({lang}facultative{/lang})" class="form-control">
		<option></option>
		<optgroup label="{lang}European Union countries{/lang}">
		  <option {if $country=='at'}selected{/if} class="at" value="at">{lang}at{/lang}</option>
		  <option {if $country=='be'}selected{/if} class="be" value="be">{lang}be{/lang}</option>
		  <option {if $country=='bg'}selected{/if} class="bg" value="bg">{lang}bg{/lang}</option>
		  <option {if $country=='hr'}selected{/if} class="hr" value="hr">{lang}hr{/lang}</option>
		  <option {if $country=='cy'}selected{/if} class="cy" value="cy">{lang}cy{/lang}</option>
		  <option {if $country=='cz'}selected{/if} class="cz" value="cz">{lang}cz{/lang}</option>
		  <option {if $country=='dk'}selected{/if} class="dk" value="dk">{lang}dk{/lang}</option>
		  <option {if $country=='ee'}selected{/if} class="ee" value="ee">{lang}ee{/lang}</option>
		  <option {if $country=='fi'}selected{/if} class="fi" value="fi">{lang}fi{/lang}</option>
		  <option {if $country=='fr'}selected{/if} class="fr" value="fr">{lang}fr{/lang}</option>
		  <option {if $country=='de'}selected{/if} class="de" value="de">{lang}de{/lang}</option>
		  <option {if $country=='gr'}selected{/if} class="gr" value="gr">{lang}gr{/lang}</option>
		  <option {if $country=='hu'}selected{/if} class="hu" value="hu">{lang}hu{/lang}</option>
		  <option {if $country=='ie'}selected{/if} class="ie" value="ie">{lang}ie{/lang}</option>
		  <option {if $country=='it'}selected{/if} class="it" value="it">{lang}it{/lang}</option>
		  <option {if $country=='lv'}selected{/if} class="lv" value="lv">{lang}lv{/lang}</option>
		  <option {if $country=='lt'}selected{/if} class="lt" value="lt">{lang}lt{/lang}</option>
		  <option {if $country=='lu'}selected{/if} class="lu" value="lu">{lang}lu{/lang}</option>
		  <option {if $country=='mt'}selected{/if} class="mt" value="mt">{lang}mt{/lang}</option>
		  <option {if $country=='nl'}selected{/if} class="nl" value="nl">{lang}nl{/lang}</option>
		  <option {if $country=='pl'}selected{/if} class="pl" value="pl">{lang}pl{/lang}</option>
		  <option {if $country=='pt'}selected{/if} class="pt" value="pt">{lang}pt{/lang}</option>
		  <option {if $country=='ro'}selected{/if} class="ro" value="ro">{lang}ro{/lang}</option>
		  <option {if $country=='sk'}selected{/if} class="sk" value="sk">{lang}sk{/lang}</option>
		  <option {if $country=='si'}selected{/if} class="si" value="si">{lang}si{/lang}</option>
		  <option {if $country=='sp'}selected{/if} class="sp" value="es">{lang}es{/lang}</option>
		  <option {if $country=='se'}selected{/if} class="se" value="se">{lang}se{/lang}</option>
		  <option {if $country=='gb'}selected{/if} class="gb" value="gb">{lang}gb{/lang}</option>
		</optgroup>

		<optgroup label="{lang}Other countries{/lang}">
		  <option {if $country=='af'}selected{/if} class="af" value="af">{lang}af{/lang}</option>
		  <option {if $country=='ax'}selected{/if} class="ax" value="ax">{lang}ax{/lang}</option>
		  <option {if $country=='al'}selected{/if} class="al" value="al">{lang}al{/lang}</option>
		  <option {if $country=='dz'}selected{/if} class="dz" value="dz">{lang}dz{/lang}</option>
		  <option {if $country=='as'}selected{/if} class="as" value="as">{lang}as{/lang}</option>
		  <option {if $country=='ad'}selected{/if} class="ad" value="ad">{lang}ad{/lang}</option>
		  <option {if $country=='ao'}selected{/if} class="ao" value="ao">{lang}ao{/lang}</option>
		  <option {if $country=='ai'}selected{/if} class="ai" value="ai">{lang}ai{/lang}</option>
		  <option {if $country=='aq'}selected{/if} class="aq" value="aq">{lang}aq{/lang}</option>
		  <option {if $country=='ag'}selected{/if} class="ag" value="ag">{lang}ag{/lang}</option>
		  <option {if $country=='ar'}selected{/if} class="ar" value="ar">{lang}ar{/lang}</option>
		  <option {if $country=='am'}selected{/if} class="am" value="am">{lang}am{/lang}</option>
		  <option {if $country=='aw'}selected{/if} class="aw" value="aw">{lang}aw{/lang}</option>
		  <option {if $country=='au'}selected{/if} class="au" value="au">{lang}au{/lang}</option>
		  <option {if $country=='az'}selected{/if} class="az" value="az">{lang}az{/lang}</option>
		  <option {if $country=='bs'}selected{/if} class="bs" value="bs">{lang}bs{/lang}</option>
		  <option {if $country=='bh'}selected{/if} class="bh" value="bh">{lang}bh{/lang}</option>
		  <option {if $country=='bd'}selected{/if} class="bd" value="bd">{lang}bd{/lang}</option>
		  <option {if $country=='bb'}selected{/if} class="bb" value="bb">{lang}bb{/lang}</option>
		  <option {if $country=='by'}selected{/if} class="by" value="by">{lang}by{/lang}</option>
		  <option {if $country=='bz'}selected{/if} class="bz" value="bz">{lang}bz{/lang}</option>
		  <option {if $country=='bj'}selected{/if} class="bj" value="bj">{lang}bj{/lang}</option>
		  <option {if $country=='bm'}selected{/if} class="bm" value="bm">{lang}bm{/lang}</option>
		  <option {if $country=='bt'}selected{/if} class="bt" value="bt">{lang}bt{/lang}</option>
		  <option {if $country=='bo'}selected{/if} class="bo" value="bo">{lang}bo{/lang}</option>
		  <option {if $country=='ba'}selected{/if} class="ba" value="ba">{lang}ba{/lang}</option>
		  <option {if $country=='bw'}selected{/if} class="bw" value="bw">{lang}bw{/lang}</option>
		  <option {if $country=='bv'}selected{/if} class="bv" value="bv">{lang}bv{/lang}</option>
		  <option {if $country=='br'}selected{/if} class="br" value="br">{lang}br{/lang}</option>
		  <option {if $country=='io'}selected{/if} class="io" value="io">{lang}io{/lang}</option>
		  <option {if $country=='bn'}selected{/if} class="bn" value="bn">{lang}bn{/lang}</option>
		  <option {if $country=='bf'}selected{/if} class="bf" value="bf">{lang}bf{/lang}</option>
		  <option {if $country=='bi'}selected{/if} class="bi" value="bi">{lang}bi{/lang}</option>
		  <option {if $country=='kh'}selected{/if} class="kh" value="kh">{lang}kh{/lang}</option>
		  <option {if $country=='cm'}selected{/if} class="cm" value="cm">{lang}cm{/lang}</option>
		  <option {if $country=='ca'}selected{/if} class="ca" value="ca">{lang}ca{/lang}</option>
		  <option {if $country=='cv'}selected{/if} class="cv" value="cv">{lang}cv{/lang}</option>
		  <option {if $country=='ky'}selected{/if} class="ky" value="ky">{lang}ky{/lang}</option>
		  <option {if $country=='cf'}selected{/if} class="cf" value="cf">{lang}cf{/lang}</option>
		  <option {if $country=='td'}selected{/if} class="td" value="td">{lang}td{/lang}</option>
		  <option {if $country=='cl'}selected{/if} class="cl" value="cl">{lang}cl{/lang}</option>
		  <option {if $country=='cn'}selected{/if} class="cn" value="cn">{lang}cn{/lang}</option>
		  <option {if $country=='cx'}selected{/if} class="cx" value="cx">{lang}cx{/lang}</option>
		  <option {if $country=='cc'}selected{/if} class="cc" value="cc">{lang}cc{/lang}</option>
		  <option {if $country=='co'}selected{/if} class="co" value="co">{lang}co{/lang}</option>
		  <option {if $country=='km'}selected{/if} class="km" value="km">{lang}km{/lang}</option>
		  <option {if $country=='cg'}selected{/if} class="cg" value="cg">{lang}cg{/lang}</option>
		  <option {if $country=='cd'}selected{/if} class="cd" value="cd">{lang}cd{/lang}</option>
		  <option {if $country=='ck'}selected{/if} class="ck" value="ck">{lang}ck{/lang}</option>
		  <option {if $country=='cr'}selected{/if} class="cr" value="cr">{lang}cr{/lang}</option>
		  <option {if $country=='ci'}selected{/if} class="ci" value="ci">{lang}ci{/lang}</option>
		  <option {if $country=='cu'}selected{/if} class="cu" value="cu">{lang}cu{/lang}</option>
		  <option {if $country=='dj'}selected{/if} class="dj" value="dj">{lang}dj{/lang}</option>
		  <option {if $country=='dm'}selected{/if} class="dm" value="dm">{lang}dm{/lang}</option>
		  <option {if $country=='do'}selected{/if} class="do" value="do">{lang}do{/lang}</option>
		  <option {if $country=='ec'}selected{/if} class="ec" value="ec">{lang}ec{/lang}</option>
		  <option {if $country=='eg'}selected{/if} class="eg" value="eg">{lang}eg{/lang}</option>
		  <option {if $country=='sv'}selected{/if} class="sv" value="sv">{lang}sv{/lang}</option>
		  <option {if $country=='gq'}selected{/if} class="gq" value="gq">{lang}gq{/lang}</option>
		  <option {if $country=='er'}selected{/if} class="er" value="er">{lang}er{/lang}</option>
		  <option {if $country=='et'}selected{/if} class="et" value="et">{lang}et{/lang}</option>
		  <option {if $country=='fk'}selected{/if} class="fk" value="fk">{lang}fk{/lang}</option>
		  <option {if $country=='fo'}selected{/if} class="fo" value="fo">{lang}fo{/lang}</option>
		  <option {if $country=='fj'}selected{/if} class="fj" value="fj">{lang}fj{/lang}</option>
		  <option {if $country=='gf'}selected{/if} class="gf" value="gf">{lang}gf{/lang}</option>
		  <option {if $country=='pf'}selected{/if} class="pf" value="pf">{lang}pf{/lang}</option>
		  <option {if $country=='tf'}selected{/if} class="tf" value="tf">{lang}tf{/lang}</option>
		  <option {if $country=='ga'}selected{/if} class="ga" value="ga">{lang}ga{/lang}</option>
		  <option {if $country=='gm'}selected{/if} class="gm" value="gm">{lang}gm{/lang}</option>
		  <option {if $country=='ge'}selected{/if} class="ge" value="ge">{lang}ge{/lang}</option>
		  <option {if $country=='gh'}selected{/if} class="gh" value="gh">{lang}gh{/lang}</option>
		  <option {if $country=='gi'}selected{/if} class="gi" value="gi">{lang}gi{/lang}</option>
		  <option {if $country=='gl'}selected{/if} class="gl" value="gl">{lang}gl{/lang}</option>
		  <option {if $country=='gd'}selected{/if} class="gd" value="gd">{lang}gd{/lang}</option>
		  <option {if $country=='gp'}selected{/if} class="gp" value="gp">{lang}gp{/lang}</option>
		  <option {if $country=='gu'}selected{/if} class="gu" value="gu">{lang}gu{/lang}</option>
		  <option {if $country=='gt'}selected{/if} class="gt" value="gt">{lang}gt{/lang}</option>
		  <option {if $country=='gg'}selected{/if} class="gg" value="gg">{lang}gg{/lang}</option>
		  <option {if $country=='gn'}selected{/if} class="gn" value="gn">{lang}gn{/lang}</option>
		  <option {if $country=='gw'}selected{/if} class="gw" value="gw">{lang}gw{/lang}</option>
		  <option {if $country=='gy'}selected{/if} class="gy" value="gy">{lang}gy{/lang}</option>
		  <option {if $country=='ht'}selected{/if} class="ht" value="ht">{lang}ht{/lang}</option>
		  <option {if $country=='hm'}selected{/if} class="hm" value="hm">{lang}hm{/lang}</option>
		  <option {if $country=='va'}selected{/if} class="va" value="va">{lang}va{/lang}</option>
		  <option {if $country=='hn'}selected{/if} class="hn" value="hn">{lang}hn{/lang}</option>
		  <option {if $country=='hk'}selected{/if} class="hk" value="hk">{lang}hk{/lang}</option>
		  <option {if $country=='is'}selected{/if} class="is" value="is">{lang}is{/lang}</option>
		  <option {if $country=='in'}selected{/if} class="in" value="in">{lang}in{/lang}</option>
		  <option {if $country=='id'}selected{/if} class="id" value="id">{lang}id{/lang}</option>
		  <option {if $country=='ir'}selected{/if} class="ir" value="ir">{lang}ir{/lang}</option>
		  <option {if $country=='iq'}selected{/if} class="iq" value="iq">{lang}iq{/lang}</option>
		  <option {if $country=='im'}selected{/if} class="im" value="im">{lang}im{/lang}</option>
		  <option {if $country=='il'}selected{/if} class="il" value="il">{lang}il{/lang}</option>
		  <option {if $country=='jm'}selected{/if} class="jm" value="jm">{lang}jm{/lang}</option>
		  <option {if $country=='jp'}selected{/if} class="jp" value="jp">{lang}jp{/lang}</option>
		  <option {if $country=='je'}selected{/if} class="je" value="je">{lang}je{/lang}</option>
		  <option {if $country=='jo'}selected{/if} class="jo" value="jo">{lang}jo{/lang}</option>
		  <option {if $country=='kz'}selected{/if} class="kz" value="kz">{lang}kz{/lang}</option>
		  <option {if $country=='ke'}selected{/if} class="ke" value="ke">{lang}ke{/lang}</option>
		  <option {if $country=='ki'}selected{/if} class="ki" value="ki">{lang}ki{/lang}</option>
		  <option {if $country=='kp'}selected{/if} class="kp" value="kp">{lang}kp{/lang}</option>
		  <option {if $country=='kr'}selected{/if} class="kr" value="kr">{lang}kr{/lang}</option>
		  <option {if $country=='kw'}selected{/if} class="kw" value="kw">{lang}kw{/lang}</option>
		  <option {if $country=='kg'}selected{/if} class="kg" value="kg">{lang}kg{/lang}</option>
		  <option {if $country=='la'}selected{/if} class="la" value="la">{lang}la{/lang}</option>
		  <option {if $country=='lb'}selected{/if} class="lb" value="lb">{lang}lb{/lang}</option>
		  <option {if $country=='ls'}selected{/if} class="ls" value="ls">{lang}ls{/lang}</option>
		  <option {if $country=='lr'}selected{/if} class="lr" value="lr">{lang}lr{/lang}</option>
		  <option {if $country=='ly'}selected{/if} class="ly" value="ly">{lang}ly{/lang}</option>
		  <option {if $country=='li'}selected{/if} class="li" value="li">{lang}li{/lang}</option>
		  <option {if $country=='mo'}selected{/if} class="mo" value="mo">{lang}mo{/lang}</option>
		  <option {if $country=='mk'}selected{/if} class="mk" value="mk">{lang}mk{/lang}</option>
		  <option {if $country=='mg'}selected{/if} class="mg" value="mg">{lang}mg{/lang}</option>
		  <option {if $country=='mw'}selected{/if} class="mw" value="mw">{lang}mw{/lang}</option>
		  <option {if $country=='my'}selected{/if} class="my" value="my">{lang}my{/lang}</option>
		  <option {if $country=='mv'}selected{/if} class="mv" value="mv">{lang}mv{/lang}</option>
		  <option {if $country=='ml'}selected{/if} class="ml" value="ml">{lang}ml{/lang}</option>
		  <option {if $country=='mh'}selected{/if} class="mh" value="mh">{lang}mh{/lang}</option>
		  <option {if $country=='mq'}selected{/if} class="mq" value="mq">{lang}mq{/lang}</option>
		  <option {if $country=='mr'}selected{/if} class="mr" value="mr">{lang}mr{/lang}</option>
		  <option {if $country=='mu'}selected{/if} class="mu" value="mu">{lang}mu{/lang}</option>
		  <option {if $country=='yt'}selected{/if} class="yt" value="yt">{lang}yt{/lang}</option>
		  <option {if $country=='mx'}selected{/if} class="mx" value="mx">{lang}mx{/lang}</option>
		  <option {if $country=='fm'}selected{/if} class="fm" value="fm">{lang}fm{/lang}</option>
		  <option {if $country=='md'}selected{/if} class="md" value="md">{lang}md{/lang}</option>
		  <option {if $country=='mc'}selected{/if} class="mc" value="mc">{lang}mc{/lang}</option>
		  <option {if $country=='mn'}selected{/if} class="mn" value="mn">{lang}mn{/lang}</option>
		  <option {if $country=='me'}selected{/if} class="me" value="me">{lang}me{/lang}</option>
		  <option {if $country=='ms'}selected{/if} class="ms" value="ms">{lang}ms{/lang}</option>
		  <option {if $country=='ma'}selected{/if} class="ma" value="ma">{lang}ma{/lang}</option>
		  <option {if $country=='mz'}selected{/if} class="mz" value="mz">{lang}mz{/lang}</option>
		  <option {if $country=='mm'}selected{/if} class="mm" value="mm">{lang}mm{/lang}</option>
		  <option {if $country=='na'}selected{/if} class="na" value="na">{lang}na{/lang}</option>
		  <option {if $country=='nr'}selected{/if} class="nr" value="nr">{lang}nr{/lang}</option>
		  <option {if $country=='np'}selected{/if} class="np" value="np">{lang}np{/lang}</option>
		  <option {if $country=='an'}selected{/if} class="an" value="an">{lang}an{/lang}</option>
		  <option {if $country=='nc'}selected{/if} class="nc" value="nc">{lang}nc{/lang}</option>
		  <option {if $country=='nz'}selected{/if} class="nz" value="nz">{lang}nz{/lang}</option>
		  <option {if $country=='ni'}selected{/if} class="ni" value="ni">{lang}ni{/lang}</option>
		  <option {if $country=='ne'}selected{/if} class="ne" value="ne">{lang}ne{/lang}</option>
		  <option {if $country=='ng'}selected{/if} class="ng" value="ng">{lang}ng{/lang}</option>
		  <option {if $country=='nu'}selected{/if} class="nu" value="nu">{lang}nu{/lang}</option>
		  <option {if $country=='nf'}selected{/if} class="nf" value="nf">{lang}nf{/lang}</option>
		  <option {if $country=='mp'}selected{/if} class="mp" value="mp">{lang}mp{/lang}</option>
		  <option {if $country=='no'}selected{/if} class="no" value="no">{lang}no{/lang}</option>
		  <option {if $country=='om'}selected{/if} class="om" value="om">{lang}om{/lang}</option>
		  <option {if $country=='pk'}selected{/if} class="pk" value="pk">{lang}pk{/lang}</option>
		  <option {if $country=='pw'}selected{/if} class="pw" value="pw">{lang}pw{/lang}</option>
		  <option {if $country=='ps'}selected{/if} class="ps" value="ps">{lang}ps{/lang}</option>
		  <option {if $country=='pa'}selected{/if} class="pa" value="pa">{lang}pa{/lang}</option>
		  <option {if $country=='pg'}selected{/if} class="pg" value="pg">{lang}pg{/lang}</option>
		  <option {if $country=='py'}selected{/if} class="py" value="py">{lang}py{/lang}</option>
		  <option {if $country=='pe'}selected{/if} class="pe" value="pe">{lang}pe{/lang}</option>
		  <option {if $country=='ph'}selected{/if} class="ph" value="ph">{lang}ph{/lang}</option>
		  <option {if $country=='pn'}selected{/if} class="pn" value="pn">{lang}pn{/lang}</option>
		  <option {if $country=='pr'}selected{/if} class="pr" value="pr">{lang}pr{/lang}</option>
		  <option {if $country=='qa'}selected{/if} class="qa" value="qa">{lang}qa{/lang}</option>
		  <option {if $country=='re'}selected{/if} class="re" value="re">{lang}re{/lang}</option>
		  <option {if $country=='ru'}selected{/if} class="ru" value="ru">{lang}ru{/lang}</option>
		  <option {if $country=='rw'}selected{/if} class="rw" value="rw">{lang}rw{/lang}</option>
		  <option {if $country=='sh'}selected{/if} class="sh" value="sh">{lang}sh{/lang}</option>
		  <option {if $country=='kn'}selected{/if} class="kn" value="kn">{lang}kn{/lang}</option>
		  <option {if $country=='lc'}selected{/if} class="lc" value="lc">{lang}lc{/lang}</option>
		  <option {if $country=='pm'}selected{/if} class="pm" value="pm">{lang}pm{/lang}</option>
		  <option {if $country=='vc'}selected{/if} class="vc" value="vc">{lang}vc{/lang}</option>
		  <option {if $country=='ws'}selected{/if} class="ws" value="ws">{lang}ws{/lang}</option>
		  <option {if $country=='sm'}selected{/if} class="sm" value="sm">{lang}sm{/lang}</option>
		  <option {if $country=='st'}selected{/if} class="st" value="st">{lang}st{/lang}</option>
		  <option {if $country=='sa'}selected{/if} class="sa" value="sa">{lang}sa{/lang}</option>
		  <option {if $country=='sn'}selected{/if} class="sn" value="sn">{lang}sn{/lang}</option>
		  <option {if $country=='rs'}selected{/if} class="rs" value="rs">{lang}rs{/lang}</option>
		  <option {if $country=='sc'}selected{/if} class="sc" value="sc">{lang}sc{/lang}</option>
		  <option {if $country=='sl'}selected{/if} class="sl" value="sl">{lang}sl{/lang}</option>
		  <option {if $country=='sg'}selected{/if} class="sg" value="sg">{lang}sg{/lang}</option>
		  <option {if $country=='sb'}selected{/if} class="sb" value="sb">{lang}sb{/lang}</option>
		  <option {if $country=='so'}selected{/if} class="so" value="so">{lang}so{/lang}</option>
		  <option {if $country=='za'}selected{/if} class="za" value="za">{lang}za{/lang}</option>
		  <option {if $country=='gs'}selected{/if} class="gs" value="gs">{lang}gs{/lang}</option>
		  <option {if $country=='lk'}selected{/if} class="lk" value="lk">{lang}lk{/lang}</option>
		  <option {if $country=='sd'}selected{/if} class="sd" value="sd">{lang}sd{/lang}</option>
		  <option {if $country=='sr'}selected{/if} class="sr" value="sr">{lang}sr{/lang}</option>
		  <option {if $country=='sj'}selected{/if} class="sj" value="sj">{lang}sj{/lang}</option>
		  <option {if $country=='sz'}selected{/if} class="sz" value="sz">{lang}sz{/lang}</option>
		  <option {if $country=='ch'}selected{/if} class="ch" value="ch">{lang}ch{/lang}</option>
		  <option {if $country=='sy'}selected{/if} class="sy" value="sy">{lang}sy{/lang}</option>
		  <option {if $country=='tw'}selected{/if} class="tw" value="tw">{lang}tw{/lang}</option>
		  <option {if $country=='tj'}selected{/if} class="tj" value="tj">{lang}tj{/lang}</option>
		  <option {if $country=='tz'}selected{/if} class="tz" value="tz">{lang}tz{/lang}</option>
		  <option {if $country=='th'}selected{/if} class="th" value="th">{lang}th{/lang}</option>
		  <option {if $country=='tl'}selected{/if} class="tl" value="tl">{lang}tl{/lang}</option>
		  <option {if $country=='tg'}selected{/if} class="tg" value="tg">{lang}tg{/lang}</option>
		  <option {if $country=='tk'}selected{/if} class="tk" value="tk">{lang}tk{/lang}</option>
		  <option {if $country=='to'}selected{/if} class="to" value="to">{lang}to{/lang}</option>
		  <option {if $country=='tt'}selected{/if} class="tt" value="tt">{lang}tt{/lang}</option>
		  <option {if $country=='tn'}selected{/if} class="tn" value="tn">{lang}tn{/lang}</option>
		  <option {if $country=='tr'}selected{/if} class="tr" value="tr">{lang}tr{/lang}</option>
		  <option {if $country=='tm'}selected{/if} class="tm" value="tm">{lang}tm{/lang}</option>
		  <option {if $country=='tc'}selected{/if} class="tc" value="tc">{lang}tc{/lang}</option>
		  <option {if $country=='tv'}selected{/if} class="tv" value="tv">{lang}tv{/lang}</option>
		  <option {if $country=='ug'}selected{/if} class="ug" value="ug">{lang}ug{/lang}</option>
		  <option {if $country=='ua'}selected{/if} class="ua" value="ua">{lang}ua{/lang}</option>
		  <option {if $country=='ae'}selected{/if} class="ae" value="ae">{lang}ae{/lang}</option>
		  <option {if $country=='us'}selected{/if} class="us" value="us">{lang}us{/lang}</option>
		  <option {if $country=='um'}selected{/if} class="um" value="um">{lang}um{/lang}</option>
		  <option {if $country=='uy'}selected{/if} class="uy" value="uy">{lang}uy{/lang}</option>
		  <option {if $country=='uz'}selected{/if} class="uz" value="uz">{lang}uz{/lang}</option>
		  <option {if $country=='vu'}selected{/if} class="vu" value="vu">{lang}vu{/lang}</option>
		  <option {if $country=='ve'}selected{/if} class="ve" value="ve">{lang}ve{/lang}</option>
		  <option {if $country=='vn'}selected{/if} class="vn" value="vn">{lang}vn{/lang}</option>
		  <option {if $country=='vg'}selected{/if} class="vg" value="vg">{lang}vg{/lang}</option>
		  <option {if $country=='vi'}selected{/if} class="vi" value="vi">{lang}vi{/lang}</option>
		  <option {if $country=='wf'}selected{/if} class="wf" value="wf">{lang}wf{/lang}</option>
		  <option {if $country=='eh'}selected{/if} class="eh" value="eh">{lang}eh{/lang}</option>
		  <option {if $country=='ye'}selected{/if} class="ye" value="ye">{lang}ye{/lang}</option>
		  <option {if $country=='zm'}selected{/if} class="zm" value="zm">{lang}zm{/lang}</option>
		  <option {if $country=='zw'}selected{/if} class="zw" value="zw">{lang}zw{/lang}</option>
		</optgroup>{if $country=='p'}selected{/if} 
	      </select>
	    </div>
	  </div>

	  <input type="submit" class="btn btn-primary" value="{lang}Confirm my signature{/lang}" />
	</div>
      </div>
    </form>
  </div>


  <div class="col-md-6">  
    <form class="form-horizontal" id="sign-individual" action="{$petition_url}validate" method="POST">
      <div class="panel panel-primary">
	<div class="panel-heading">
	  <h3 class="panel-title">{lang}I am a citizen{/lang}</h3>
	</div>
	<div class="panel-body">
	  <input type="hidden" name="signature_type" value="0" />
	  <input type="hidden" name="email_set" value="{$email_set}" />

	  <div class="form-group form-group-sm {if $signature_type == 0 && $errors.firstname}has-error{/if}">
	    <label for="firstname" class="col-sm-5 control-label">{lang}Firstname{/lang}</label>
	    <div class="col-sm-7">
	      <input class="form-control" type="text"
		     name="firstname" 
		     placeholder="{lang}Firstname{/lang} ({lang}mandatory{/lang})" 
		     {if $firstname}value="{$firstname}"{/if} />
	    </div>
	  </div>

	  <div class="form-group form-group-sm {if $signature_type == 0 && $errors.name}has-error{/if}">
	    <label for="name" class="col-sm-5 control-label">{lang}Name{/lang}</label>
	    <div class="col-sm-7">
	      <input class="form-control" type="text"
		     name="name" 
		     placeholder="{lang}Name{/lang} ({lang}mandatory{/lang})" 
		     {if $name}value="{$name}"{/if} />
	    </div>
	  </div>

	  <div class="form-group form-group-sm {if $signature_type == 0 && $errors.occupation}has-error{/if}">
	    <label for="occupation" class="col-sm-5 control-label">{lang}Occupation{/lang}</label>
	    <div class="col-sm-7">
	      <input class="form-control" type="text"
		     occupation="occupation" 
		     placeholder="{lang}Occupation{/lang} ({lang}facultative{/lang}, {lang}if applicable{/lang})" 
		     {if $occupation}value="{$occupation}"{/if} />
	    </div>
	  </div>

	  {if ! $email_set || ! $email }
	  <div class="form-group form-group-sm {if $signature_type == 0 && $errors.email}has-error{/if}">
	    <label for="email" class="col-sm-5 control-label">{lang}Email{/lang}</label>
	    <div class="col-sm-7">
	      <input class="form-control" type="text" 
		     name="email" 
		     placeholder="{lang}Email{/lang} ({lang}mandatory{/lang})" 
		     {if $email}value="{$email}"{/if} />
	    </div>
	  </div>
	  {else}
	  <input type="hidden" name="email" value="{$email}" />
	  {/if}
	  <div class="form-group form-group-sm {if $signature_type == 1 && $errors.country}has-error{/if}">
	    <label for="country" class="col-sm-5 control-label">{lang}Country{/lang}</label>
	    <div class="col-sm-7">
	      <select name="country" id="countries-2" data-placeholder="{lang}Choose your country{/lang} ({lang}facultative{/lang})" style="width:300px;">
		<option></option>
		<optgroup label="{lang}European countries{/lang}">
		  <option {if $country=='at'}selected{/if} class="at" value="at">{lang}at{/lang}</option>
		  <option {if $country=='be'}selected{/if} class="be" value="be">{lang}be{/lang}</option>
		  <option {if $country=='bg'}selected{/if} class="bg" value="bg">{lang}bg{/lang}</option>
		  <option {if $country=='hr'}selected{/if} class="hr" value="hr">{lang}hr{/lang}</option>
		  <option {if $country=='cy'}selected{/if} class="cy" value="cy">{lang}cy{/lang}</option>
		  <option {if $country=='cz'}selected{/if} class="cz" value="cz">{lang}cz{/lang}</option>
		  <option {if $country=='dk'}selected{/if} class="dk" value="dk">{lang}dk{/lang}</option>
		  <option {if $country=='ee'}selected{/if} class="ee" value="ee">{lang}ee{/lang}</option>
		  <option {if $country=='fi'}selected{/if} class="fi" value="fi">{lang}fi{/lang}</option>
		  <option {if $country=='fr'}selected{/if} class="fr" value="fr">{lang}fr{/lang}</option>
		  <option {if $country=='de'}selected{/if} class="de" value="de">{lang}de{/lang}</option>
		  <option {if $country=='gr'}selected{/if} class="gr" value="gr">{lang}gr{/lang}</option>
		  <option {if $country=='hu'}selected{/if} class="hu" value="hu">{lang}hu{/lang}</option>
		  <option {if $country=='ie'}selected{/if} class="ie" value="ie">{lang}ie{/lang}</option>
		  <option {if $country=='it'}selected{/if} class="it" value="it">{lang}it{/lang}</option>
		  <option {if $country=='lv'}selected{/if} class="lv" value="lv">{lang}lv{/lang}</option>
		  <option {if $country=='lt'}selected{/if} class="lt" value="lt">{lang}lt{/lang}</option>
		  <option {if $country=='lu'}selected{/if} class="lu" value="lu">{lang}lu{/lang}</option>
		  <option {if $country=='mt'}selected{/if} class="mt" value="mt">{lang}mt{/lang}</option>
		  <option {if $country=='nl'}selected{/if} class="nl" value="nl">{lang}nl{/lang}</option>
		  <option {if $country=='pl'}selected{/if} class="pl" value="pl">{lang}pl{/lang}</option>
		  <option {if $country=='pt'}selected{/if} class="pt" value="pt">{lang}pt{/lang}</option>
		  <option {if $country=='ro'}selected{/if} class="ro" value="ro">{lang}ro{/lang}</option>
		  <option {if $country=='sk'}selected{/if} class="sk" value="sk">{lang}sk{/lang}</option>
		  <option {if $country=='si'}selected{/if} class="si" value="si">{lang}si{/lang}</option>
		  <option {if $country=='sp'}selected{/if} class="sp" value="es">{lang}es{/lang}</option>
		  <option {if $country=='se'}selected{/if} class="se" value="se">{lang}se{/lang}</option>
		  <option {if $country=='gb'}selected{/if} class="gb" value="gb">{lang}gb{/lang}</option>
		</optgroup>

		<optgroup label="{lang}Other countries{/lang}">
		  <option {if $country=='af'}selected{/if} class="af" value="af">{lang}af{/lang}</option>
		  <option {if $country=='ax'}selected{/if} class="ax" value="ax">{lang}ax{/lang}</option>
		  <option {if $country=='al'}selected{/if} class="al" value="al">{lang}al{/lang}</option>
		  <option {if $country=='dz'}selected{/if} class="dz" value="dz">{lang}dz{/lang}</option>
		  <option {if $country=='as'}selected{/if} class="as" value="as">{lang}as{/lang}</option>
		  <option {if $country=='ad'}selected{/if} class="ad" value="ad">{lang}ad{/lang}</option>
		  <option {if $country=='ao'}selected{/if} class="ao" value="ao">{lang}ao{/lang}</option>
		  <option {if $country=='ai'}selected{/if} class="ai" value="ai">{lang}ai{/lang}</option>
		  <option {if $country=='aq'}selected{/if} class="aq" value="aq">{lang}aq{/lang}</option>
		  <option {if $country=='ag'}selected{/if} class="ag" value="ag">{lang}ag{/lang}</option>
		  <option {if $country=='ar'}selected{/if} class="ar" value="ar">{lang}ar{/lang}</option>
		  <option {if $country=='am'}selected{/if} class="am" value="am">{lang}am{/lang}</option>
		  <option {if $country=='aw'}selected{/if} class="aw" value="aw">{lang}aw{/lang}</option>
		  <option {if $country=='au'}selected{/if} class="au" value="au">{lang}au{/lang}</option>
		  <option {if $country=='az'}selected{/if} class="az" value="az">{lang}az{/lang}</option>
		  <option {if $country=='bs'}selected{/if} class="bs" value="bs">{lang}bs{/lang}</option>
		  <option {if $country=='bh'}selected{/if} class="bh" value="bh">{lang}bh{/lang}</option>
		  <option {if $country=='bd'}selected{/if} class="bd" value="bd">{lang}bd{/lang}</option>
		  <option {if $country=='bb'}selected{/if} class="bb" value="bb">{lang}bb{/lang}</option>
		  <option {if $country=='by'}selected{/if} class="by" value="by">{lang}by{/lang}</option>
		  <option {if $country=='bz'}selected{/if} class="bz" value="bz">{lang}bz{/lang}</option>
		  <option {if $country=='bj'}selected{/if} class="bj" value="bj">{lang}bj{/lang}</option>
		  <option {if $country=='bm'}selected{/if} class="bm" value="bm">{lang}bm{/lang}</option>
		  <option {if $country=='bt'}selected{/if} class="bt" value="bt">{lang}bt{/lang}</option>
		  <option {if $country=='bo'}selected{/if} class="bo" value="bo">{lang}bo{/lang}</option>
		  <option {if $country=='ba'}selected{/if} class="ba" value="ba">{lang}ba{/lang}</option>
		  <option {if $country=='bw'}selected{/if} class="bw" value="bw">{lang}bw{/lang}</option>
		  <option {if $country=='bv'}selected{/if} class="bv" value="bv">{lang}bv{/lang}</option>
		  <option {if $country=='br'}selected{/if} class="br" value="br">{lang}br{/lang}</option>
		  <option {if $country=='io'}selected{/if} class="io" value="io">{lang}io{/lang}</option>
		  <option {if $country=='bn'}selected{/if} class="bn" value="bn">{lang}bn{/lang}</option>
		  <option {if $country=='bf'}selected{/if} class="bf" value="bf">{lang}bf{/lang}</option>
		  <option {if $country=='bi'}selected{/if} class="bi" value="bi">{lang}bi{/lang}</option>
		  <option {if $country=='kh'}selected{/if} class="kh" value="kh">{lang}kh{/lang}</option>
		  <option {if $country=='cm'}selected{/if} class="cm" value="cm">{lang}cm{/lang}</option>
		  <option {if $country=='ca'}selected{/if} class="ca" value="ca">{lang}ca{/lang}</option>
		  <option {if $country=='cv'}selected{/if} class="cv" value="cv">{lang}cv{/lang}</option>
		  <option {if $country=='ky'}selected{/if} class="ky" value="ky">{lang}ky{/lang}</option>
		  <option {if $country=='cf'}selected{/if} class="cf" value="cf">{lang}cf{/lang}</option>
		  <option {if $country=='td'}selected{/if} class="td" value="td">{lang}td{/lang}</option>
		  <option {if $country=='cl'}selected{/if} class="cl" value="cl">{lang}cl{/lang}</option>
		  <option {if $country=='cn'}selected{/if} class="cn" value="cn">{lang}cn{/lang}</option>
		  <option {if $country=='cx'}selected{/if} class="cx" value="cx">{lang}cx{/lang}</option>
		  <option {if $country=='cc'}selected{/if} class="cc" value="cc">{lang}cc{/lang}</option>
		  <option {if $country=='co'}selected{/if} class="co" value="co">{lang}co{/lang}</option>
		  <option {if $country=='km'}selected{/if} class="km" value="km">{lang}km{/lang}</option>
		  <option {if $country=='cg'}selected{/if} class="cg" value="cg">{lang}cg{/lang}</option>
		  <option {if $country=='cd'}selected{/if} class="cd" value="cd">{lang}cd{/lang}</option>
		  <option {if $country=='ck'}selected{/if} class="ck" value="ck">{lang}ck{/lang}</option>
		  <option {if $country=='cr'}selected{/if} class="cr" value="cr">{lang}cr{/lang}</option>
		  <option {if $country=='ci'}selected{/if} class="ci" value="ci">{lang}ci{/lang}</option>
		  <option {if $country=='cu'}selected{/if} class="cu" value="cu">{lang}cu{/lang}</option>
		  <option {if $country=='dj'}selected{/if} class="dj" value="dj">{lang}dj{/lang}</option>
		  <option {if $country=='dm'}selected{/if} class="dm" value="dm">{lang}dm{/lang}</option>
		  <option {if $country=='do'}selected{/if} class="do" value="do">{lang}do{/lang}</option>
		  <option {if $country=='ec'}selected{/if} class="ec" value="ec">{lang}ec{/lang}</option>
		  <option {if $country=='eg'}selected{/if} class="eg" value="eg">{lang}eg{/lang}</option>
		  <option {if $country=='sv'}selected{/if} class="sv" value="sv">{lang}sv{/lang}</option>
		  <option {if $country=='gq'}selected{/if} class="gq" value="gq">{lang}gq{/lang}</option>
		  <option {if $country=='er'}selected{/if} class="er" value="er">{lang}er{/lang}</option>
		  <option {if $country=='et'}selected{/if} class="et" value="et">{lang}et{/lang}</option>
		  <option {if $country=='fk'}selected{/if} class="fk" value="fk">{lang}fk{/lang}</option>
		  <option {if $country=='fo'}selected{/if} class="fo" value="fo">{lang}fo{/lang}</option>
		  <option {if $country=='fj'}selected{/if} class="fj" value="fj">{lang}fj{/lang}</option>
		  <option {if $country=='gf'}selected{/if} class="gf" value="gf">{lang}gf{/lang}</option>
		  <option {if $country=='pf'}selected{/if} class="pf" value="pf">{lang}pf{/lang}</option>
		  <option {if $country=='tf'}selected{/if} class="tf" value="tf">{lang}tf{/lang}</option>
		  <option {if $country=='ga'}selected{/if} class="ga" value="ga">{lang}ga{/lang}</option>
		  <option {if $country=='gm'}selected{/if} class="gm" value="gm">{lang}gm{/lang}</option>
		  <option {if $country=='ge'}selected{/if} class="ge" value="ge">{lang}ge{/lang}</option>
		  <option {if $country=='gh'}selected{/if} class="gh" value="gh">{lang}gh{/lang}</option>
		  <option {if $country=='gi'}selected{/if} class="gi" value="gi">{lang}gi{/lang}</option>
		  <option {if $country=='gl'}selected{/if} class="gl" value="gl">{lang}gl{/lang}</option>
		  <option {if $country=='gd'}selected{/if} class="gd" value="gd">{lang}gd{/lang}</option>
		  <option {if $country=='gp'}selected{/if} class="gp" value="gp">{lang}gp{/lang}</option>
		  <option {if $country=='gu'}selected{/if} class="gu" value="gu">{lang}gu{/lang}</option>
		  <option {if $country=='gt'}selected{/if} class="gt" value="gt">{lang}gt{/lang}</option>
		  <option {if $country=='gg'}selected{/if} class="gg" value="gg">{lang}gg{/lang}</option>
		  <option {if $country=='gn'}selected{/if} class="gn" value="gn">{lang}gn{/lang}</option>
		  <option {if $country=='gw'}selected{/if} class="gw" value="gw">{lang}gw{/lang}</option>
		  <option {if $country=='gy'}selected{/if} class="gy" value="gy">{lang}gy{/lang}</option>
		  <option {if $country=='ht'}selected{/if} class="ht" value="ht">{lang}ht{/lang}</option>
		  <option {if $country=='hm'}selected{/if} class="hm" value="hm">{lang}hm{/lang}</option>
		  <option {if $country=='va'}selected{/if} class="va" value="va">{lang}va{/lang}</option>
		  <option {if $country=='hn'}selected{/if} class="hn" value="hn">{lang}hn{/lang}</option>
		  <option {if $country=='hk'}selected{/if} class="hk" value="hk">{lang}hk{/lang}</option>
		  <option {if $country=='is'}selected{/if} class="is" value="is">{lang}is{/lang}</option>
		  <option {if $country=='in'}selected{/if} class="in" value="in">{lang}in{/lang}</option>
		  <option {if $country=='id'}selected{/if} class="id" value="id">{lang}id{/lang}</option>
		  <option {if $country=='ir'}selected{/if} class="ir" value="ir">{lang}ir{/lang}</option>
		  <option {if $country=='iq'}selected{/if} class="iq" value="iq">{lang}iq{/lang}</option>
		  <option {if $country=='im'}selected{/if} class="im" value="im">{lang}im{/lang}</option>
		  <option {if $country=='il'}selected{/if} class="il" value="il">{lang}il{/lang}</option>
		  <option {if $country=='jm'}selected{/if} class="jm" value="jm">{lang}jm{/lang}</option>
		  <option {if $country=='jp'}selected{/if} class="jp" value="jp">{lang}jp{/lang}</option>
		  <option {if $country=='je'}selected{/if} class="je" value="je">{lang}je{/lang}</option>
		  <option {if $country=='jo'}selected{/if} class="jo" value="jo">{lang}jo{/lang}</option>
		  <option {if $country=='kz'}selected{/if} class="kz" value="kz">{lang}kz{/lang}</option>
		  <option {if $country=='ke'}selected{/if} class="ke" value="ke">{lang}ke{/lang}</option>
		  <option {if $country=='ki'}selected{/if} class="ki" value="ki">{lang}ki{/lang}</option>
		  <option {if $country=='kp'}selected{/if} class="kp" value="kp">{lang}kp{/lang}</option>
		  <option {if $country=='kr'}selected{/if} class="kr" value="kr">{lang}kr{/lang}</option>
		  <option {if $country=='kw'}selected{/if} class="kw" value="kw">{lang}kw{/lang}</option>
		  <option {if $country=='kg'}selected{/if} class="kg" value="kg">{lang}kg{/lang}</option>
		  <option {if $country=='la'}selected{/if} class="la" value="la">{lang}la{/lang}</option>
		  <option {if $country=='lb'}selected{/if} class="lb" value="lb">{lang}lb{/lang}</option>
		  <option {if $country=='ls'}selected{/if} class="ls" value="ls">{lang}ls{/lang}</option>
		  <option {if $country=='lr'}selected{/if} class="lr" value="lr">{lang}lr{/lang}</option>
		  <option {if $country=='ly'}selected{/if} class="ly" value="ly">{lang}ly{/lang}</option>
		  <option {if $country=='li'}selected{/if} class="li" value="li">{lang}li{/lang}</option>
		  <option {if $country=='mo'}selected{/if} class="mo" value="mo">{lang}mo{/lang}</option>
		  <option {if $country=='mk'}selected{/if} class="mk" value="mk">{lang}mk{/lang}</option>
		  <option {if $country=='mg'}selected{/if} class="mg" value="mg">{lang}mg{/lang}</option>
		  <option {if $country=='mw'}selected{/if} class="mw" value="mw">{lang}mw{/lang}</option>
		  <option {if $country=='my'}selected{/if} class="my" value="my">{lang}my{/lang}</option>
		  <option {if $country=='mv'}selected{/if} class="mv" value="mv">{lang}mv{/lang}</option>
		  <option {if $country=='ml'}selected{/if} class="ml" value="ml">{lang}ml{/lang}</option>
		  <option {if $country=='mh'}selected{/if} class="mh" value="mh">{lang}mh{/lang}</option>
		  <option {if $country=='mq'}selected{/if} class="mq" value="mq">{lang}mq{/lang}</option>
		  <option {if $country=='mr'}selected{/if} class="mr" value="mr">{lang}mr{/lang}</option>
		  <option {if $country=='mu'}selected{/if} class="mu" value="mu">{lang}mu{/lang}</option>
		  <option {if $country=='yt'}selected{/if} class="yt" value="yt">{lang}yt{/lang}</option>
		  <option {if $country=='mx'}selected{/if} class="mx" value="mx">{lang}mx{/lang}</option>
		  <option {if $country=='fm'}selected{/if} class="fm" value="fm">{lang}fm{/lang}</option>
		  <option {if $country=='md'}selected{/if} class="md" value="md">{lang}md{/lang}</option>
		  <option {if $country=='mc'}selected{/if} class="mc" value="mc">{lang}mc{/lang}</option>
		  <option {if $country=='mn'}selected{/if} class="mn" value="mn">{lang}mn{/lang}</option>
		  <option {if $country=='me'}selected{/if} class="me" value="me">{lang}me{/lang}</option>
		  <option {if $country=='ms'}selected{/if} class="ms" value="ms">{lang}ms{/lang}</option>
		  <option {if $country=='ma'}selected{/if} class="ma" value="ma">{lang}ma{/lang}</option>
		  <option {if $country=='mz'}selected{/if} class="mz" value="mz">{lang}mz{/lang}</option>
		  <option {if $country=='mm'}selected{/if} class="mm" value="mm">{lang}mm{/lang}</option>
		  <option {if $country=='na'}selected{/if} class="na" value="na">{lang}na{/lang}</option>
		  <option {if $country=='nr'}selected{/if} class="nr" value="nr">{lang}nr{/lang}</option>
		  <option {if $country=='np'}selected{/if} class="np" value="np">{lang}np{/lang}</option>
		  <option {if $country=='an'}selected{/if} class="an" value="an">{lang}an{/lang}</option>
		  <option {if $country=='nc'}selected{/if} class="nc" value="nc">{lang}nc{/lang}</option>
		  <option {if $country=='nz'}selected{/if} class="nz" value="nz">{lang}nz{/lang}</option>
		  <option {if $country=='ni'}selected{/if} class="ni" value="ni">{lang}ni{/lang}</option>
		  <option {if $country=='ne'}selected{/if} class="ne" value="ne">{lang}ne{/lang}</option>
		  <option {if $country=='ng'}selected{/if} class="ng" value="ng">{lang}ng{/lang}</option>
		  <option {if $country=='nu'}selected{/if} class="nu" value="nu">{lang}nu{/lang}</option>
		  <option {if $country=='nf'}selected{/if} class="nf" value="nf">{lang}nf{/lang}</option>
		  <option {if $country=='mp'}selected{/if} class="mp" value="mp">{lang}mp{/lang}</option>
		  <option {if $country=='no'}selected{/if} class="no" value="no">{lang}no{/lang}</option>
		  <option {if $country=='om'}selected{/if} class="om" value="om">{lang}om{/lang}</option>
		  <option {if $country=='pk'}selected{/if} class="pk" value="pk">{lang}pk{/lang}</option>
		  <option {if $country=='pw'}selected{/if} class="pw" value="pw">{lang}pw{/lang}</option>
		  <option {if $country=='ps'}selected{/if} class="ps" value="ps">{lang}ps{/lang}</option>
		  <option {if $country=='pa'}selected{/if} class="pa" value="pa">{lang}pa{/lang}</option>
		  <option {if $country=='pg'}selected{/if} class="pg" value="pg">{lang}pg{/lang}</option>
		  <option {if $country=='py'}selected{/if} class="py" value="py">{lang}py{/lang}</option>
		  <option {if $country=='pe'}selected{/if} class="pe" value="pe">{lang}pe{/lang}</option>
		  <option {if $country=='ph'}selected{/if} class="ph" value="ph">{lang}ph{/lang}</option>
		  <option {if $country=='pn'}selected{/if} class="pn" value="pn">{lang}pn{/lang}</option>
		  <option {if $country=='pr'}selected{/if} class="pr" value="pr">{lang}pr{/lang}</option>
		  <option {if $country=='qa'}selected{/if} class="qa" value="qa">{lang}qa{/lang}</option>
		  <option {if $country=='re'}selected{/if} class="re" value="re">{lang}re{/lang}</option>
		  <option {if $country=='ru'}selected{/if} class="ru" value="ru">{lang}ru{/lang}</option>
		  <option {if $country=='rw'}selected{/if} class="rw" value="rw">{lang}rw{/lang}</option>
		  <option {if $country=='sh'}selected{/if} class="sh" value="sh">{lang}sh{/lang}</option>
		  <option {if $country=='kn'}selected{/if} class="kn" value="kn">{lang}kn{/lang}</option>
		  <option {if $country=='lc'}selected{/if} class="lc" value="lc">{lang}lc{/lang}</option>
		  <option {if $country=='pm'}selected{/if} class="pm" value="pm">{lang}pm{/lang}</option>
		  <option {if $country=='vc'}selected{/if} class="vc" value="vc">{lang}vc{/lang}</option>
		  <option {if $country=='ws'}selected{/if} class="ws" value="ws">{lang}ws{/lang}</option>
		  <option {if $country=='sm'}selected{/if} class="sm" value="sm">{lang}sm{/lang}</option>
		  <option {if $country=='st'}selected{/if} class="st" value="st">{lang}st{/lang}</option>
		  <option {if $country=='sa'}selected{/if} class="sa" value="sa">{lang}sa{/lang}</option>
		  <option {if $country=='sn'}selected{/if} class="sn" value="sn">{lang}sn{/lang}</option>
		  <option {if $country=='rs'}selected{/if} class="rs" value="rs">{lang}rs{/lang}</option>
		  <option {if $country=='sc'}selected{/if} class="sc" value="sc">{lang}sc{/lang}</option>
		  <option {if $country=='sl'}selected{/if} class="sl" value="sl">{lang}sl{/lang}</option>
		  <option {if $country=='sg'}selected{/if} class="sg" value="sg">{lang}sg{/lang}</option>
		  <option {if $country=='sb'}selected{/if} class="sb" value="sb">{lang}sb{/lang}</option>
		  <option {if $country=='so'}selected{/if} class="so" value="so">{lang}so{/lang}</option>
		  <option {if $country=='za'}selected{/if} class="za" value="za">{lang}za{/lang}</option>
		  <option {if $country=='gs'}selected{/if} class="gs" value="gs">{lang}gs{/lang}</option>
		  <option {if $country=='lk'}selected{/if} class="lk" value="lk">{lang}lk{/lang}</option>
		  <option {if $country=='sd'}selected{/if} class="sd" value="sd">{lang}sd{/lang}</option>
		  <option {if $country=='sr'}selected{/if} class="sr" value="sr">{lang}sr{/lang}</option>
		  <option {if $country=='sj'}selected{/if} class="sj" value="sj">{lang}sj{/lang}</option>
		  <option {if $country=='sz'}selected{/if} class="sz" value="sz">{lang}sz{/lang}</option>
		  <option {if $country=='ch'}selected{/if} class="ch" value="ch">{lang}ch{/lang}</option>
		  <option {if $country=='sy'}selected{/if} class="sy" value="sy">{lang}sy{/lang}</option>
		  <option {if $country=='tw'}selected{/if} class="tw" value="tw">{lang}tw{/lang}</option>
		  <option {if $country=='tj'}selected{/if} class="tj" value="tj">{lang}tj{/lang}</option>
		  <option {if $country=='tz'}selected{/if} class="tz" value="tz">{lang}tz{/lang}</option>
		  <option {if $country=='th'}selected{/if} class="th" value="th">{lang}th{/lang}</option>
		  <option {if $country=='tl'}selected{/if} class="tl" value="tl">{lang}tl{/lang}</option>
		  <option {if $country=='tg'}selected{/if} class="tg" value="tg">{lang}tg{/lang}</option>
		  <option {if $country=='tk'}selected{/if} class="tk" value="tk">{lang}tk{/lang}</option>
		  <option {if $country=='to'}selected{/if} class="to" value="to">{lang}to{/lang}</option>
		  <option {if $country=='tt'}selected{/if} class="tt" value="tt">{lang}tt{/lang}</option>
		  <option {if $country=='tn'}selected{/if} class="tn" value="tn">{lang}tn{/lang}</option>
		  <option {if $country=='tr'}selected{/if} class="tr" value="tr">{lang}tr{/lang}</option>
		  <option {if $country=='tm'}selected{/if} class="tm" value="tm">{lang}tm{/lang}</option>
		  <option {if $country=='tc'}selected{/if} class="tc" value="tc">{lang}tc{/lang}</option>
		  <option {if $country=='tv'}selected{/if} class="tv" value="tv">{lang}tv{/lang}</option>
		  <option {if $country=='ug'}selected{/if} class="ug" value="ug">{lang}ug{/lang}</option>
		  <option {if $country=='ua'}selected{/if} class="ua" value="ua">{lang}ua{/lang}</option>
		  <option {if $country=='ae'}selected{/if} class="ae" value="ae">{lang}ae{/lang}</option>
		  <option {if $country=='us'}selected{/if} class="us" value="us">{lang}us{/lang}</option>
		  <option {if $country=='um'}selected{/if} class="um" value="um">{lang}um{/lang}</option>
		  <option {if $country=='uy'}selected{/if} class="uy" value="uy">{lang}uy{/lang}</option>
		  <option {if $country=='uz'}selected{/if} class="uz" value="uz">{lang}uz{/lang}</option>
		  <option {if $country=='vu'}selected{/if} class="vu" value="vu">{lang}vu{/lang}</option>
		  <option {if $country=='ve'}selected{/if} class="ve" value="ve">{lang}ve{/lang}</option>
		  <option {if $country=='vn'}selected{/if} class="vn" value="vn">{lang}vn{/lang}</option>
		  <option {if $country=='vg'}selected{/if} class="vg" value="vg">{lang}vg{/lang}</option>
		  <option {if $country=='vi'}selected{/if} class="vi" value="vi">{lang}vi{/lang}</option>
		  <option {if $country=='wf'}selected{/if} class="wf" value="wf">{lang}wf{/lang}</option>
		  <option {if $country=='eh'}selected{/if} class="eh" value="eh">{lang}eh{/lang}</option>
		  <option {if $country=='ye'}selected{/if} class="ye" value="ye">{lang}ye{/lang}</option>
		  <option {if $country=='zm'}selected{/if} class="zm" value="zm">{lang}zm{/lang}</option>
		  <option {if $country=='zw'}selected{/if} class="zw" value="zw">{lang}zw{/lang}</option>
		</optgroup>
	      </select>
	    </div>
	  </div>

	  <div class="checkbox">
	    <label>
	      <input type="checkbox" name="keep_mail" id="keep_mail" {if
		     $keep_mail}checked="true"{/if} value="1" />
	      {lang}Keep me informed of future campaigns{/lang}
	    </label>
	  </div>

	  <div class="checkbox">
	    <label>
	      <input type="checkbox" name="hide_signature"
		     id="hide_signature" {if
		     $hide_signature}checked="true"{/if} value="1" />
	      {lang}My signature is private (hide my name){/lang}
	    </label>
	  </div>

	  <input type="submit" class="btn btn-primary" style="margin-top:1em" value="{lang}Confirm my signature{/lang}" />

	</div>
      </div>
    </form>
    <p>{lang}<b>Note:</b> We will not publish or share any of your information with any party outside us.{/lang}</p>
  </div>
</div>

  <script>
{literal}
$(document).ready(function() {
	$("#countries-1, #countries-2").chosen({no_results_text: "{/literal}{lang}No country matched:{/lang}{literal}"});
})
{/literal}
  </script>

{include file="footer.tpl"}
