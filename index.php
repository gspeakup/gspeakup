<?php // -*- c++ -*-

/* ************************************************************************** */
/*                                                                            */
/*     Copyright (C) 2007-2014 Benjamin Drieu (bdrieu@april.org)	      */
/*                                                                            */
/*  This program is free software; you can redistribute it and/or modify      */
/*  it under the terms of the GNU General Public License as published by      */
/*  the Free Software Foundation; either version 2 of the License, or         */
/*  (at your option) any later version.                                       */
/*                                                                            */
/*  This program is distributed in the hope that it will be useful,           */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU General Public License for more details.                              */
/*                                                                            */
/*  You should have received a copy of the GNU General Public License         */
/*  along with this program; if not, write to the Free Software               */
/*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */
/*                                                                            */
/* ************************************************************************** */


require 'include/options.php';

// Set session to two days
session_set_cookie_params ( 172800, '/', GPT_DOMAIN );
session_name ( 'GPTSESSID' );
session_start ();

require './include/utils.php';
require './include/language.php';
require './include/mail.php';
require './include/petition.php';

require './libs/classOrgile.php';



$info = $_SERVER [ 'PATH_INFO' ];
$info = preg_replace ( '/\/\//', '/', $info );
$info = preg_replace ( '/^\/*/', '', $info );
$path_info = @split ( '/', $info );
$action = $path_info [ 0 ];
$what = @$path_info [ 1 ];
$object = @$path_info [ 2 ];

if ( $action != 'images' )
  header("Content-Type: text/html; charset=utf-8");
 else
   header("Content-Type: image/png");


if ( ( ! array_key_exists ( 'referer', $_SESSION ) || 
       $_SESSION [ 'referer' ] == '' ) &&
     array_key_exists ( 'HTTP_REFERER', $_SERVER ) &&
     ! strpos ( $_SERVER [ 'HTTP_REFERER' ], $_SERVER [ 'HTTP_HOST' ] ) )
  {
    $_SESSION [ 'referer' ] = $_SERVER [ 'HTTP_REFERER' ];
  }

if ( array_key_exists ( 'dc_language_default', $_COOKIE ) )
  {
    $_SESSION [ 'language' ] = $_COOKIE [ 'dc_language_default' ];
  }
 else if ( array_key_exists ( 'HTTP_ACCEPT_LANGUAGE', $_SERVER ) )
   {
     $_SESSION [ 'language' ] = find_browser_language ( $_SERVER [ 'HTTP_ACCEPT_LANGUAGE' ] );
   }
   
// i18n.  Default to english and then overload stuff.
include ( './lang/default.php' );
if ( file_exists ( './lang/en.php' ) )
  include ( './lang/en.php' );
if ( array_key_exists ( 'language', $_SESSION ) && 
     file_exists ( './lang/' . $_SESSION [ 'language' ] . '.php' ) )
{
    include ( './lang/' . $_SESSION [ 'language' ] . '.php' );
}

if ( array_key_exists ( 'action', $_GET ) && $_GET [ 'action' ] == 'set_language' )
  {
    set_language ( $_GET [ 'language' ] );
    exit;
  }

$smarty = init_smarty ( null, '.' );
$smarty -> assign ( 'gpt_mail_bot', htmlspecialchars(GPT_MAIL_BOT) );
define ( 'PETITION_URL', GPT_BASE_URL );
$smarty -> assign ( 'petition_url', GPT_BASE_URL );
if ( defined ( 'PETITION_MAP' ) )
  $smarty -> assign ( 'petition_map', PETITION_MAP );

$smarty -> assign ( 'action', $action );
$smarty -> assign ( 'object', $object );
$smarty -> assign ( 'what', $what );

if ( $action == 'sign' )
  {
    if ( array_key_exists ( 'email_set', $_POST ) )
      $smarty -> assign ( 'email_set', $_POST [ 'email_set' ] );
    if ( array_key_exists ( 'email', $_POST ) )
      $smarty -> assign ( 'email', $_POST [ 'email' ] );
    $smarty -> display ( 'sign.tpl' );
  }
 else if ( $action == 'validate' )
   {
     if ( petition_check_signature ( $_POST ) )
       {
	 petition_validate ( $_POST );
       }
     else
       {
	 foreach ( Array ( 'email_set', 'country', 'email', 'firstname', 'name', 
			   'organization_name', 'organization_description', 'organization_website',
			   'keep_mail', 'show_signature', 'signature_type', 'occupation') as $arg )
	   {
	     if ( array_key_exists ( $arg, $_POST ) )
	       $smarty -> assign ( $arg, $_POST [ $arg ] );
	   }
	 $smarty -> display ( 'sign.tpl' );
       }
   }
 else if ( $action == 'confirm' )
   {
     petition_confirm ( $what );
   }
 else if ( $action == 'cancel' )
   {
     petition_cancel ( $what );
   }
 else if ( $action == 'visibility' )
   {
     petition_visibility ( $what );
   }
 else if ( $action == 'signatures' )
   {
     petition_show_signatures ( $what, $object, 2 );
   }
 else if ( $action == 'signatures-individuals' )
   {
     petition_show_signatures ( $what, $object, 0 );
   }
 else if ( $action == 'signatures-organizations' )
   {
     petition_show_signatures ( $what, $object, 1 );
   }
 else if ( $action == 'page' )
   {
     petition_show_page ( $what );
   }
 else if ( $action )
   {
     if ( $action == 'images' && $what == 'flags' )
       {
	 print ( file_get_contents ( 'images/flags/unk.png' ) );
	 exit;
       }
     croak ( "Unknown page. Have you followed a bad link?" );
   }
 else 
   {
     $signatories = simple_unique_query ( "SELECT COUNT(signature_id) AS count FROM signature WHERE validated_time IS NOT NULL;" );
     $smarty -> assign ( 'signatories', $signatories['count'] );
     if ( defined ( 'PETITION_GOAL' ) )
       {
	 $smarty -> assign ( 'petition_goal', PETITION_GOAL );
	 
	 $smarty -> assign ( 'petition_percentage', sprintf ( "%.2f", $signatories['count'] / PETITION_GOAL * 100 ));
       }

     $base_countries = Array ('at','be','bg','cy','cz','de','dk','ee','es','fi','fr','gb','gr','hr','hu','ie','it','lt','lu','lv','mt','nl','pl','pt','ro','se','si','sk' );
     $smarty -> assign ( 'base_countries', $base_countries );

     foreach ( Array ( 'ad', 'ae', 'af', 'ag', 'ai', 'al', 'am', 'ao', 'aq',
		       'ar', 'as', 'at', 'au', 'aw', 'ax', 'az', 'ba', 'bb',
		       'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bl', 'bm',
		       'bn', 'bo', 'bq', 'br', 'bs', 'bt', 'bv', 'bw', 'by',
		       'bz', 'ca', 'cc', 'cd', 'cf', 'cg', 'ch', 'ci', 'ck',
		       'cl', 'cm', 'cn', 'co', 'cr', 'cu', 'cv', 'cw', 'cx',
		       'cy', 'cz', 'de', 'dj', 'dk', 'dm', 'do', 'dz', 'ec',
		       'ee', 'eg', 'eh', 'er', 'es', 'et', 'fi', 'fj', 'fk',
		       'fm', 'fo', 'fr', 'ga', 'gb', 'gd', 'ge', 'gf', 'gg',
		       'gh', 'gi', 'gl', 'gm', 'gn', 'gp', 'gq', 'gr', 'gs',
		       'gt', 'gu', 'gw', 'gy', 'hk', 'hm', 'hn', 'hr', 'ht',
		       'hu', 'id', 'ie', 'il', 'im', 'in', 'io', 'iq', 'ir',
		       'is', 'it', 'je', 'jm', 'jo', 'jp', 'ke', 'kg', 'kh',
		       'ki', 'km', 'kn', 'kp', 'kr', 'kw', 'ky', 'kz', 'la',
		       'lb', 'lc', 'li', 'lk', 'lr', 'ls', 'lt', 'lu', 'lv',
		       'ly', 'ma', 'mc', 'md', 'me', 'mf', 'mg', 'mh', 'mk',
		       'ml', 'mm', 'mn', 'mo', 'mp', 'mq', 'mr', 'ms', 'mt',
		       'mu', 'mv', 'mw', 'mx', 'my', 'mz', 'na', 'nc', 'ne',
		       'nf', 'ng', 'ni', 'nl', 'no', 'np', 'nr', 'nu', 'nz',
		       'om', 'pa', 'pe', 'pf', 'pg', 'ph', 'pk', 'pl', 'pm',
		       'pn', 'pr', 'ps', 'pt', 'pw', 'py', 'qa', 're', 'ro',
		       'rs', 'ru', 'rw', 'sa', 'sb', 'sc', 'sd', 'se', 'sg',
		       'sh', 'si', 'sj', 'sk', 'sl', 'sm', 'sn', 'so', 'sr',
		       'ss', 'st', 'sv', 'sx', 'sy', 'sz', 'tc', 'td', 'tf',
		       'tg', 'th', 'tj', 'tk', 'tl', 'tm', 'tn', 'to', 'tr',
		       'tt', 'tv', 'tw', 'tz', 'ua', 'ug', 'um', 'us', 'uy',
		       'uz', 'va', 'vc', 've', 'vg', 'vi', 'vn', 'vu', 'wf',
		       'ws', 'ye', 'yt', 'za', 'zm', 'zw' ) as $code )
       {
	 $all_countries [ ] = Array ( 'code' => $code, 'name' => lang ( $code ) );
       }
     $smarty -> assign ( 'all_countries', $all_countries );

     $countries = simple_unique_query ( "SELECT COUNT(DISTINCT country) AS count FROM signature WHERE validated_time;" );
     $smarty -> assign ( 'countries', $countries['count'] );

     $by_country = simple_query ( "SELECT country AS code, COUNT(signature_id) AS count FROM signature WHERE validated_time IS NOT NULL GROUP BY country;" );
     $smarty -> assign ( 'by_country', $by_country );

     $smarty -> assign ( 'latest_signatories', 
			 simple_query ( "SELECT *, UNIX_TIMESTAMP(validated_time) AS epoch, TIMEDIFF(NOW(),validated_time) AS time_ago FROM signature WHERE validated_time IS NOT NULL ORDER BY validated_time DESC LIMIT 20;" ) );
     $smarty -> assign ( 'first_signatories', 
			 simple_query ( "SELECT *, UNIX_TIMESTAMP(validated_time) AS epoch, TIMEDIFF(NOW(),validated_time) AS time_ago FROM signature WHERE validated_time IS NOT NULL ORDER BY validated_time ASC LIMIT 100;" ) );
     $smarty -> display ( 'index.tpl' );
   }
?>
