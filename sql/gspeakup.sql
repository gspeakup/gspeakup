CREATE TABLE signature (
       `signature_id` INT(11) NOT NULL auto_increment,  
       `updated_time` TIMESTAMP NOT NULL,

       `signature_type` INT NOT NULL,

       `validated_time` DATETIME DEFAULT NULL,
       `signed_time` DATETIME DEFAULT NULL,

       `ip` INT UNSIGNED NOT NULL,
       `ip_country` VARCHAR(2),
       `referer` VARCHAR(128),
       `key` VARCHAR(41),

       `email` VARCHAR(128) NOT NULL,
       `firstname` VARCHAR(64),
       `occupation` VARCHAR(64),
       `name` VARCHAR(128),
       `country` VARCHAR(2),

       `keep_mail` BOOLEAN NOT NULL,
       `show_signature` BOOLEAN NOT NULL,

       `organization_name` VARCHAR(256),
       `organization_description` TEXT,
       `organization_website` VARCHAR(256),
       
  PRIMARY KEY  (`signature_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE signature ADD INDEX ( signed_time );
ALTER TABLE signature ADD INDEX ( signature_type );
ALTER TABLE signature ADD INDEX ( firstname );
ALTER TABLE signature ADD INDEX ( name );
